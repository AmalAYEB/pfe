<?php

namespace SL\UsersBundle\Security;

use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use SL\UsersBundle\Form\LoginForm;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Security;


class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    private $em;

    private $router;

    private $passwordEncoder;

    //Create a form in the authenticator
    public function __construct(FormFactoryInterface $formFactory,EntityManager $em, RouterInterface $router, UserPasswordEncoder $passwordEncoder)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;

    }
    //This method sees  if the URL is /login and the HTTP method is POST and return the form data
    public function getCredentials(Request $request)
    {
        $isLoginSubmit = $request->getPathInfo() == '/' && $request->isMethod('POST');
        if (!$isLoginSubmit) {
            // skip authentication
            //If it returns null from getCredentials(), Symfony skips trying to authenticate the user and the request continues on like normal.
            return;
        }
        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);
        $data = $form->getData();
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $data['login']
        );
        return $data;
    }
    //This method extract data from database according to the login
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['login'];
        //Login times
        $user = $this->em->getRepository('SLUsersBundle:User')->findOneBy(['login' => $username]);
        $user->setFirstlog(1);
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['_password'];
        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            return true;
        }
        return false;
    }
    //Redirect the user to the login form when the authentication is failed
    protected function getLoginUrl()
    {
        return $this->router->generate('security_login');
    }
    //Redirect the user to the homepage when the authentication is successful
    protected function getDefaultSuccessRedirectUrl()
    {
        return $this->router->generate('homepage');
    }
}