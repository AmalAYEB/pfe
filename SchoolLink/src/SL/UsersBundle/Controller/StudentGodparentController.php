<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\StudentGodparent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

class StudentGodparentController extends Controller
{
    function editAction($relid,$relation){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getEntityManager();
        $rel = $em->getRepository('SLUsersBundle:StudentGodparent')->find($relid);
        $rel->setRelationtype($relation);
        $em->persist($rel);
        $em->flush();
        $response = new Response();
        $response->setContent(json_encode(array(
            'relation' => $serializer->serialize($rel, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function addAction($stid,$gpid,$relation){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getEntityManager();
        $student = $em->getRepository('SLUsersBundle:Student')->find($stid);
        $godparent = $em->getRepository('SLUsersBundle:Godparent')->find($gpid);
        $rel= new StudentGodparent();
        $rel->setRelationtype($relation);
        $rel->setGodparent($godparent);
        $rel->setStudent($student);
        $em->persist($rel);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array(
            'relation' => $serializer->serialize($rel, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function deleteAction($relid){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $relation = $em->getRepository('SLUsersBundle:StudentGodparent')->find($relid);
        $godparent = $em->getRepository('SLUsersBundle:Godparent')->find($relation->getGodparent());
        $em->remove($relation);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array(
            'godparent' => $serializer->serialize($godparent, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
