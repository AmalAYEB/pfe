<?php

namespace SL\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SL\UsersBundle\Form\StudentRegistrationForm;
use SL\UsersBundle\Entity\Student;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends Controller
{
    public function addAction(Request $request,$classid)
    {
        $student = new Student();
        $em = $this->getDoctrine()->getManager();
        $class = $em->getRepository('SLUsersBundle:Stclass')->find($classid);
        $student->setStclass($class);
        $form = $this->createForm(StudentRegistrationForm::class, $student, array(
            'action' => $this->generateUrl('add_student',array('classid'=>$classid))));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form["picture"]->getData() != null) {
                $file = $student->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
              $student->setPicture($fileName);
            }
            $student->getStschool($this->getUser()->getSchool());
            $student->setLastActivity(new \DateTime('now'));
            $em->persist($student);
            $em->flush();
            return $this->redirectToRoute('view_class',array('classid' => $classid));
        }
        return $this->render(
            'SLUsersBundle:Users/Admin:addstudent.html.twig',
            array('form' => $form->createView())
        );
    }
    public function listAction($classid,$sender){
        $em = $this->getDoctrine()->getManager();
        $class = $em->getRepository('SLUsersBundle:Stclass')->find($classid);
        $students = $em->getRepository('SLUsersBundle:Student')->findBy(array('stclass' => $class), null, null, null);
        $stgp=array(NULL);
        $i=0;
        foreach($students as $student) {
            $stgp[$i] = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('student' => $student), null, null, null);
            $i++;
        }
        if($sender=="admin") {
            return $this->render('SLUsersBundle:Users/Admin:liststudents.html.twig', array(
                'students' => $students,'parents'=>$stgp
            ));
        }else{

            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $response = new Response();
            $response->setContent(json_encode(array(
                'students' => $serializer->serialize($students, 'json'),
                'parents' => $serializer->serialize($stgp, 'json'),
            )));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }

    public function showAction($studentid){
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('SLUsersBundle:Student')->find($studentid);
        return $this->render('SLUsersBundle:Users/Admin:showstudent.html.twig', array(
            'student' => $student,
        ));
    }

    public function deleteAction($studentid)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('SLUsersBundle:Student')->find($studentid);
        $relations= $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('student' => $student), null, null, null);
        $class= $student->getStclass();
        $em->remove($student);
        foreach($relations as $rel) {
            $em->remove($rel);
        }
        $em->flush();

        return $this->render('SLUsersBundle:Users/Admin:viewclassAdmin.html.twig',array('class'=>$class));
    }


    public function editAction(Request $request,$studentid, $page)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $student = $em->getRepository('SLUsersBundle:Student')->find($studentid);
        $pic = $student->getPicture();
        $editForm = $this->createForm(StudentRegistrationForm::class, $student, array(
            'action' => $this->generateUrl('edit_student', array('studentid' => $studentid,'page'=>$page))));

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editForm["picture"]->getData() != null) {
                $file = $student->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
                $student->setPicture($fileName);
            } else {
                $student->setPicture($pic);
            }
            $editForm->getData();
            $em->persist($student);
            $em->flush();
            if($page=='student'){
                return $this->redirect($_SERVER['HTTP_REFERER']);
            }
        }
        if($page=='student') {
            return $this->render('SLUsersBundle:Users:Student/editStudent.html.twig', [
                'student' => $student,
                'form' => $editForm->createView()
            ]);
        }else{
            return $this->render('SLUsersBundle:Users/Admin:editstudent.html.twig', [
                'student' => $student,
                'form' => $editForm->createView()
            ]);
        }
    }

    function chatlistGodparentAction ($classeid){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $em = $this->getDoctrine()->getManager();

        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $admin=$em->getRepository('SLUsersBundle:Admin')->findBy(array('school' => $teacher->getSchoolidTeacher()),null ,null, null);
        $class=$em->getRepository('SLUsersBundle:Stclass')->find($classeid);
        $students = $em->getRepository('SLUsersBundle:Student')->findBy(array('stclass' => $class), null, null, null);
        $stgp=array(NULL);
        $i=0;
        foreach($students as $student) {
            $stgp[$i] = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('student' => $student), null, null, null);
            $i++;
        }
        $response = new Response();
        $response->setContent(json_encode(array(
            'stgp' => $serializer->serialize($stgp, 'json'),
            'admin' => $serializer->serialize($admin, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function passAction($studentid,$classid){
        $em = $this->getDoctrine()->getManager();
        $student=$em->getRepository('SLUsersBundle:Student')->find($studentid);
        $class=$em->getRepository('SLUsersBundle:Stclass')->find($classid);
        $student->setStclass($class);
        $em->persist($student);
        $em->flush();

        return $this->render('SLUsersBundle:Users/Admin:viewclassAdmin.html.twig',array('class'=>$class));
    }

    public function menuAction()
    {

        $list = array(
            array('title' => "Accueil", 'path' => 'homepage','icon'=>"fa fa-home sidebar-nav-icon"),
            array('title' => "Avancement des cours", 'path' => 'course_progress','icon'=>"fa fa-file-text sidebar-nav-icon"),
            array('title' => "Cahier de texte", 'path' => 'textbook_student','icon'=>"fa fa-list-alt  sidebar-nav-icon"),
        );

        return $this->render('SLUsersBundle:Users:menu.html.twig', array(
            'list' => $list
        ));
    }

    public function courseprogressAction(){
        $em = $this->getDoctrine()->getManager();
        $year=$this->getUser()->getStschool()->getYear();
        $courses= $em->getRepository('SLUsersBundle:Course')->findByDateLImitCourse($year);
        $class=$em->getRepository('SLUsersBundle:Stclass')->find($this->getUser()->getStclass());
        $subjects=$em->getRepository('SLUsersBundle:Subject')->findBy(array('level' => $class->getLevel()), null, null, null);
        return $this->render('SLUsersBundle:Users/Student:courses.html.twig',array('class' => $class,'subjects'=> $subjects,
            'courses' => $courses));
    }

    public function textbookAction(){

        $em = $this->getDoctrine()->getManager();
        $year=$this->getUser()->getStschool()->getYear();
        $evaluations= $em->getRepository('SLUsersBundle:Evaluation')->findByDateLImitEval($year);
        $activities= $em->getRepository('SLUsersBundle:Activity')->findByDateLImitActv($year);
        $class=$em->getRepository('SLUsersBundle:Stclass')->find($this->getUser()->getStclass());

        return $this->render('SLUsersBundle:Users/Student:textbook.html.twig',array('class' => $class,
            'evaluations' => $evaluations,'activities'=>$activities
        ));
    }
}
