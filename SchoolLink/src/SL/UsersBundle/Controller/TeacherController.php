<?php

namespace SL\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SL\UsersBundle\Form\TeacherRegistrationForm;
use SL\UsersBundle\Entity\Teacher;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

class TeacherController extends Controller
{
    public function registerAction(Request $request)
    {
        $teacher = new Teacher();
        $form = $this->createForm(TeacherRegistrationForm::class, $teacher,array(
            'action' => $this->generateUrl('add_teacher')));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form["picture"]->getData() != null) {
                $file = $teacher->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
                $teacher->setPicture($fileName);
            }
            $teacher->setSchoolidTeacher($this->getUser()->getSchool());
            $teacher->setLastActivity(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($teacher);
            $em->flush();
            return $this->redirectToRoute('adminteach');
            $this->addFlash('notice', 'Success');
        }else{
            $this->addFlash('notice', 'Not Success');
        }
        return $this->render(
            'SLUsersBundle:Users/Teacher:registerTeacher.html.twig',
            array('form' => $form->createView())
        );
    }

    public function editAction(Request $request,$teacherid,$page)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $teacher = $em->getRepository('SLUsersBundle:Teacher')->find($teacherid);
        $pic=$teacher->getPicture();
        $editForm = $this->createForm(TeacherRegistrationForm::class,$teacher, array(
            'action' => $this->generateUrl('edit_teacher', array('teacherid' => $teacherid,'page'=>$page))));

        $editForm->handleRequest($request);

        if ( $editForm->isValid()) {
            if ($editForm["picture"]->getData() != null) {
                $file = $teacher->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
                $teacher->setPicture($fileName);
            }else{
                $teacher->setPicture($pic);
            }
            $editForm->getData();
            $em->persist($teacher);
            $em->flush();
            if($page=='teacher'){
            return $this->redirect($_SERVER['HTTP_REFERER']);
            }
        }
        if($page=='teacher') {
            return $this->render('SLUsersBundle:Users:Teacher/editTeacher.html.twig', [
                'teacher' => $teacher,
                'form' => $editForm->createView()
            ]);
        }else{
            return $this->render('SLUsersBundle:Users/Admin:editteacher.html.twig', [
                'teacher' => $teacher,
                'form' => $editForm->createView()
            ]);
        }
    }

    public function viewAction($teacherid)
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('SLUsersBundle:Teacher')->find($teacherid);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array('teacher' => $serializer->serialize($teacher, 'json'))));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function listAction(){
        $em = $this->getDoctrine()->getManager();
        $teachers = $em->getRepository('SLUsersBundle:Teacher')->findBy(array('schoolid_teacher' => $this->getUser()->getSchool()), null, null, null);
        return $this->render('SLUsersBundle:Users/Admin:listteachers.html.twig', array(
            'teachers' => $teachers,
        ));
    }

    public function deleteAction($teacherid)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('SLUsersBundle:Teacher')->find($teacherid);
        $send = $em->getRepository('SLUsersBundle:Message')->findBy(array('sender' => $teacher), null, null, null);
        $reciev = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' => $teacher), null, null, null);
        $inters=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher), null, null, null);
        $courses=$em->getRepository('SLUsersBundle:Course')->findBy(array('teacher' => $teacher), null, null, null);
        $evaluations=$em->getRepository('SLUsersBundle:Evaluation')->findBy(array('teacher' => $teacher), null, null, null);
        $activities=$em->getRepository('SLUsersBundle:Activity')->findBy(array('teacher' => $teacher), null, null, null);
        $publications=$em->getRepository('SLUsersBundle:Publication')->findBy(array('creator' => $teacher), null, null, null);
        if($publications!=null){
            foreach ($publications as $pub) {
                $em->remove($pub);
            }
        }
        if($activities!=null){
            foreach ($activities as $a) {
                $em->remove($a);
            }
        }
        if($evaluations!=null){
            foreach ($evaluations as $e) {
                $em->remove($e);
            }
        }
        if($courses!=null){
            foreach ($courses as $c) {
                $em->remove($c);
            }
        }
        if($inters!=null){
            foreach ($inters as $i) {
                $em->remove($i);
            }
        }
        if($reciev!=null){
            foreach ($reciev as $r) {
                $em->remove($r);
            }
        }
        if($send!=null){
            foreach ($send as $s) {
                $em->remove($s);
            }
        }
        $em->remove($teacher);
        $em->flush();

        return $this->redirectToRoute('adminteach');
    }

    public function menuAction()
    {

        $list = array(
            array('title' => "Accueil", 'path' => 'homepage','icon'=>"fa fa-home sidebar-nav-icon"),
            array('title' => "Journal", 'path' => 'diary','icon'=>"fa fa-file-text sidebar-nav-icon"),
            array('title' => "Cahier de texte", 'path' => 'textbook_teacher','icon'=>"fa fa-list-alt  sidebar-nav-icon"),
            array('title' => "Élèves", 'path' => 'teacher_students','icon'=>"fa fa-users sidebar-nav-icon")
        );

        return $this->render('SLUsersBundle:Users:menu.html.twig', array(
            'list' => $list
        ));
    }

    public function textbookAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $year=$teacher->getSchoolidTeacher()->getYear();
        $evaluations= $em->getRepository('SLUsersBundle:Evaluation')->findByDateLImitEval($year);
        $activities= $em->getRepository('SLUsersBundle:Activity')->findByDateLImitActv($year);
        $inters=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);

        return $this->render('SLUsersBundle:Users/Teacher:textbook.html.twig', array('inters' => $inters,
            'evaluations' => $evaluations,'activities'=>$activities
        ));

    }
    public function diaryAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        $year=$teacher->getSchoolidTeacher()->getYear();
        $courses=$em->getRepository('SLUsersBundle:Course')->findByDateLImitCourse($year);
        return $this->render('SLUsersBundle:Users/Teacher:diary.html.twig',array('inters'=>$inter,'courses'=>$courses));
    }

    public function studentslistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        return $this->render('SLUsersBundle:Users/Teacher:students.html.twig',array('inters'=>$inter));
    }

    public function chatlistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        return $this->render('SLUsersBundle:Users/Teacher:chatlist.html.twig',array('inters'=>$inter));
    }
}
