<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\Activity;
use SL\UsersBundle\Entity\Evaluation;
use SL\UsersBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Session\Session;

class TaskController extends Controller
{
    public function addtaskAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        $task = new Task();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,$task,array(
            'action' => $this->generateUrl('add_task')));
        $form=$formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($request->request->get('task-type')=="evaluation"){
                $classes=$request->request->get('classes');
                foreach ( $classes as $class){
                $eval=new Evaluation();
                $eval->setTitle($request->request->get('title'));
                $eval->setDescription($request->request->get('description'));
                $eval->setDate(\DateTime::createFromFormat('d/m/Y',$request->request->get('date')));
                $sub=$em->getRepository('SLUsersBundle:Subject')->find($request->request->get('subject'));
                $eval->setSubject($sub);
                $eval->setTeacher($teacher);
                $class=$em->getRepository('SLUsersBundle:Stclass')->find($class);
                $eval->setClass($class);
                $em->persist($eval);
                $em->flush();
                }
            }elseif ($request->request->get('task-type')=="activity") {
                $classes = $request->request->get('classes');
                if(is_array($classes)){
                foreach ($classes as $class) {
                    $act = new Activity();
                    $act->setTitle($request->request->get('title'));
                    $act->setDescription($request->request->get('description'));
                    if($request->request->get('rep')=="onlyonce"){
                    $act->setDate(\DateTime::createFromFormat('d/m/Y', $request->request->get('dateonce')));
                    }else{
                        $act->setDate(\DateTime::createFromFormat('d/m/Y', $request->request->get('start')));
                        $session = new Session();
                        $session->getFlashBag()->add('notice', $request->request->get('end'));
                        $act->setEnd(\DateTime::createFromFormat('d/m/Y', $request->request->get('end')));
                    }
                    $act->setRepetition($request->request->get('rep'));
                    $sub = $em->getRepository('SLUsersBundle:Subject')->find($request->request->get('subject'));
                    $act->setSubject($sub);
                    $act->setTeacher($teacher);
                    $class = $em->getRepository('SLUsersBundle:Stclass')->find($class);
                    $act->setClass($class);
                    $em->persist($act);
                    $em->flush();
                }
                }
            }
            return $this->redirectToRoute('textbook_teacher');
        }
        return $this->render(
            'SLUsersBundle:Users/Teacher:addtask.html.twig',
            array('form' => $form->createView(),'inters'=>$inter)
        );
    }

    public function deleteAction($taskid,$tasktype)
    {
        $em = $this->getDoctrine()->getManager();
        if($tasktype=="eval"){
            $evaluation = $em->getRepository('SLUsersBundle:Evaluation')->find($taskid);
            $em->remove($evaluation);
        }elseif ($tasktype=="actv"){
            $activity = $em->getRepository('SLUsersBundle:Activity')->find($taskid);
            $em->remove($activity);
        }
        $em->flush();

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function editAction($taskid,$tasktype,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        if($tasktype=="eval"){
            $task = $em->getRepository('SLUsersBundle:Evaluation')->find($taskid);
            $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,$task,array(
                'action' => $this->generateUrl('edit_task',array('taskid'=>$taskid, 'tasktype'=>"eval"))));
        }elseif ($tasktype=="actv"){
            $task = $em->getRepository('SLUsersBundle:Activity')->find($taskid);
            $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,$task,array(
                'action' => $this->generateUrl('edit_task',array('taskid'=>$taskid, 'tasktype'=>"actv"))));
        }
        $form=$formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $session->getFlashBag()->add('notice', 'dddd');
            if($tasktype=="eval"){
                    $task->setTitle($request->request->get('title'));
                    $task->setDescription($request->request->get('description'));
                    $task->setDate(\DateTime::createFromFormat('d/m/Y',$request->request->get('date')));
                    $sub=$em->getRepository('SLUsersBundle:Subject')->find($request->request->get('subject'));
                    $task->setSubject($sub);
                    $em->persist($task);
                    $em->flush();
            }elseif ($tasktype=="actv") {

                        $task->setTitle($request->request->get('title'));
                        $task->setDescription($request->request->get('description'));
                        if($request->request->get('rep')=="onlyonce"){
                            $task->setDate(\DateTime::createFromFormat('d/m/Y', $request->request->get('dateonce')));
                        }else{
                            $task->setDate(\DateTime::createFromFormat('d/m/Y', $request->request->get('start')));
                            $task->setEnd(\DateTime::createFromFormat('d/m/Y', $request->request->get('end')));
                        }
                        $task->setRepetition($request->request->get('rep'));
                        $sub = $em->getRepository('SLUsersBundle:Subject')->find($request->request->get('subject'));
                        $task->setSubject($sub);
                        $em->persist($task);
                        $em->flush();
            }
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }
        return $this->render(
            'SLUsersBundle:Users/Teacher:edittask.html.twig',
            array('form' => $form->createView(),'inters'=>$inter,'task'=>$task,'tasktype'=>$tasktype)
        );

    }
     public function showEvalAction(){
         $em = $this->getDoctrine()->getManager();
         $year=$this->getUser()->getStschool()->getYear();
         $evaluations= $em->getRepository('SLUsersBundle:Evaluation')->findByDateLImitEval($year);
         return $this->render(
             'SLUsersBundle:Users/Student:showevaluations.html.twig',
             array('evaluations'=>$evaluations)
         );
     }

}
