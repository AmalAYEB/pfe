<?php

namespace SL\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SL\UsersBundle\Entity\Publication;
use SL\UsersBundle\Entity\Media;
use SL\UsersBundle\Form\PublicationForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PublicationController extends Controller
{
    public function showAction($user)
    {
        $em = $this->getDoctrine()->getManager();
        if($user=='admin') {
            $publications = $em->getRepository('SLUsersBundle:Publication')->findBy(array('school' => $this->getUser()->getSchool()), array('datepub' => 'desc'), null, null);
        }elseif ($user=='teacher'){
            $publications = $em->getRepository('SLUsersBundle:Publication')->findBy(array('school' => $this->getUser()->getSchoolidTeacher()), array('datepub' => 'desc'), null, null);
        }elseif ($user=='godparent'){
            $publications = $em->getRepository('SLUsersBundle:Publication')->findBy(array('school' => $this->getUser()->getGdschool()), array('datepub' => 'desc'), null, null);
        }else{

            $publications = $em->getRepository('SLUsersBundle:Publication')->findBy(array('school' => $this->getUser()->getStschool() ), array('datepub' => 'desc'), null, null);
        }
        $medias = $em->getRepository('SLUsersBundle:Media')->findAll();

        return $this->render('SLUsersBundle:Users/Admin:showpublications.html.twig', array(
            'publications' => $publications,
            'medias' => $medias,
        ));
    }

    public function addAction(Request $request,$user)
    {
        $em = $this->getDoctrine()->getManager();
        $publication = new Publication();
        $media = new Media;
        $form = $this->createForm(PublicationForm::class, $media, array(

            'action' => $this->generateUrl('add_publication',array('user'=>$user))));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $files = $media->getContents();
            $images = array();
            if ($form["contents"]->getData() != null) {
                $key=0;
                foreach ($files as $file) {
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('publications_directory'), $fileName);
                    $images[$key++] = $fileName;
                }
                $media->setContents($images);
                $media->setAlt('image');
                $media->setType('img');
                $media->setPublication($publication);
                $em->persist($media);
            }
            $publication->setContents($request->request->get('publication'));
            $publication->setDatepub(new \DateTime('now'));
            $publication->setCreator($this->getUser());
            if($user=='admin') {
                $publication->setSchool($this->getUser()->getSchool());
            }else{
                $publication->setSchool($this->getUser()->getSchoolidTeacher());
            }
            $em->persist($publication);
            $em->flush();
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        return $this->render(
            'SLUsersBundle:Users/Admin:addpublication.html.twig',
            array('form' => $form->createView())
        );
    }

    public function deleteAction($pub)
    {
        $em = $this->getDoctrine()->getManager();
        $publication = $em->getRepository('SLUsersBundle:Publication')->find($pub);
        $medias = $em->getRepository('SLUsersBundle:Media')->findBy(array('publication' => $publication), null, null, null);
        foreach ($medias as $media) {
            $em->remove($media);
        }
        $em->remove($publication);
        $em->flush();

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function editAction($pubid,$content){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getEntityManager();
        $pub = $em->getRepository('SLUsersBundle:Publication')->find($pubid);
        $pub->setContents($content);
        $em->persist($pub);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array(
            'pub' => $serializer->serialize($pub, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}