<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Form\AdminRegistrationForm;
use SL\UsersBundle\Form\ProfilAdminForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public function registerAction(Request $request)
    {
        $form = $this->createForm(AdminRegistrationForm::class);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $admin = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($admin);
            $em->flush();
            $this->addFlash('success', 'Welcome '.$admin->getFirstname());
            return $this->redirectToRoute('homepage');
        }
        return $this->render('SLUsersBundle:Users:registerAdmin.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $admin = $this->getUser();
        $pic=$admin->getPicture();
        $editForm = $this->createForm(ProfilAdminForm::class,$admin, array(
            'action' => $this->generateUrl('edit_admin')));

        $editForm->handleRequest($request);

        if ( $editForm->isValid()) {
            if ($editForm["picture"]->getData() != null) {
                $file = $admin->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
                $admin->setPicture($fileName);
            }else{
                $admin->setPicture($pic);
            }
            $editForm->getData();
            $em->persist($admin);
            $em->flush();
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        return $this->render('SLUsersBundle:Users:Admin/editadmin.html.twig', [
            'admin' => $admin,
            'form' => $editForm->createView()
        ]);
    }


    public function menuAction()
    {

        $list = array(
            array('title' => "Accueil", 'path' => 'homepage','icon'=>"fa fa-home sidebar-nav-icon"),
            array('title' => "Classes", 'path' => 'classes','icon'=>"fa fa-object-group sidebar-nav-icon"),
            array('title' => "Matières", 'path' => 'subjects','icon'=>"fa fa-folder sidebar-nav-icon"),
            array('title' => "Enseignants", 'path' => 'adminteach','icon'=>"fa fa-graduation-cap sidebar-nav-icon"),
            array('title' => "Parents", 'path' => 'adminparents','icon'=>"fa fa-users sidebar-nav-icon")
        );

        return $this->render('SLUsersBundle:Users:menu.html.twig', array(
            'list' => $list
        ));
    }

    public function classesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), null, null, null);
        return $this->render('SLUsersBundle:Users/Admin:classesAdmin.html.twig',array('levels' => $levels));
    }

    public function subjectsAction()
    {
        return $this->render('SLUsersBundle:Users/Admin:subjectsAdmin.html.twig');
    }

    public function teachersAction()
    {
        return $this->render('SLUsersBundle:Users/Admin:teachersAdmin.html.twig');
    }

    public function parentsAction()
    {
        return $this->render('SLUsersBundle:Users/Admin:parentsAdmin.html.twig');
    }

    public function chatlistAction(){

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $em = $this->getDoctrine()->getManager();

        $teachers=$em->getRepository('SLUsersBundle:Teacher')->findBy(array('schoolid_teacher' => $this->getUser()->getSchool()),null ,null, null);
        $godparents = $em->getRepository('SLUsersBundle:Godparent')->findBy(array('gdschool' => $this->getUser()->getSchool()), null, null, null);

        $response = new Response();
        $response->setContent(json_encode(array(
            'parents' => $serializer->serialize($godparents, 'json'),
            'teachers' => $serializer->serialize($teachers, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function showallAction($users){
        $em = $this->getDoctrine()->getManager();
        if($users=="teachers"){
            $members=$em->getRepository('SLUsersBundle:Teacher')->findBy(array('schoolid_teacher' => $this->getUser()->getSchool()),null ,null, null);
        }elseif ($users=="parents"){
            $members=$em->getRepository('SLUsersBundle:Godparent')->findBy(array('gdschool' => $this->getUser()->getSchool()), null, null, null);
        }else{
            $members=$em->getRepository('SLUsersBundle:Student')->findBy(array('stschool' => $this->getUser()->getSchool()), null, null, null);
        }
        return $this->render('SLUsersBundle:Users/Admin:Allusers.html.twig',array('members'=>$members,"type"=>$users));
    }
}
