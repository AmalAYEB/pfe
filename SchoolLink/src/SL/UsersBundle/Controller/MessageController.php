<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Security\Core\Role\SwitchUserRole;


class MessageController extends Controller
{
    public function addAction(Request $request)
    {
        if ($request->isXMLHttpRequest()) {
            if(isset($_POST['chatMsg'])){
                $em = $this->getDoctrine()->getManager();
                $receiver=$em->getRepository('SLUsersBundle:User')->find($_POST['receiverid']);
                $message = new Message();
                $message->setDate(new \DateTime('now'));
                $message->setUnread(0);

                $authChecker = $this->get('security.authorization_checker');
                $tokenStorage = $this->get('security.token_storage');
                if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
                    foreach ($tokenStorage->getToken()->getRoles() as $role) {
                        if ($role instanceof SwitchUserRole) {
                            $gp = $role->getSource()->getUser();
                            break;
                        }
                    }
                    $message->setSender($gp);
                }else{
                    $message->setSender($this->getUser());
                }

                $message->setReceiver($receiver);
                $message->setContent($_POST['chatMsg']);
                $em->persist($message);
                $em->flush();
                return new JsonResponse(array('data' => 'this is a json response'));
        }

            return new Response('This is not ajax!', 400);
    }
    }

    public function showAction($receiverid){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $receiver=$em->getRepository('SLUsersBundle:User')->find($receiverid);

        $authChecker = $this->get('security.authorization_checker');
        $tokenStorage = $this->get('security.token_storage');
        if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
            foreach ($tokenStorage->getToken()->getRoles() as $role) {
                if ($role instanceof SwitchUserRole) {
                    $gp = $role->getSource()->getUser();
                    break;
                }
            }
            $messages = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' => array($receiver, $gp), 'sender' => array($gp, $receiver)), array('date' => 'asc'), null, null);
            $unreads = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' =>  $gp, 'sender' =>  $receiver,'unread' => 0), null, null, null);
        }else{
            $messages = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' => array($receiver, $this->getUser()), 'sender' => array($this->getUser(), $receiver)), array('date' => 'asc'), null, null);
            $unreads = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' => $this->getUser(), 'sender' =>  $receiver,'unread' => 0),null, null, null);
        }
        foreach($unreads as $unread) {
            $unread->setUnread(1);
            $em->persist($unread);
        }
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array(
            'msgs' => $serializer->serialize($messages, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }

    public function unreadAction($receiverid){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $receiver=$em->getRepository('SLUsersBundle:User')->find($receiverid);

        $authChecker = $this->get('security.authorization_checker');
        $tokenStorage = $this->get('security.token_storage');
        if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
            foreach ($tokenStorage->getToken()->getRoles() as $role) {
                if ($role instanceof SwitchUserRole) {
                    $gp = $role->getSource()->getUser();
                    break;
                }
            }
            $unreads = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' =>  $gp, 'sender' =>  $receiver,'unread' => 0), null, null, null);
        }else{
            $unreads = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' => $this->getUser(), 'sender' =>  $receiver,'unread' => 0),null, null, null);
        }
        $response = new Response();
        $response->setContent(json_encode(array(
            'unreads' => $serializer->serialize($unreads, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

    }
}
