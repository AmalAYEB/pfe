<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\Course;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Response;

class CourseController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        $course = new Course();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,$course,array(
            'action' => $this->generateUrl('add_course')));
        $form=$formBuilder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $classes=$request->request->get('classes');
            foreach ( $classes as $class){
                $course = new Course();
                $course->setTitle($request->request->get('title'));
                $course->setDescription($request->request->get('description'));
                $course->setCreation(new \DateTime('today'));
                $sub=$em->getRepository('SLUsersBundle:Subject')->find($request->request->get('subject'));
                $course->setSubject($sub);
                $course->setTeacher($teacher);
                $course->setDone(false);
                $class=$em->getRepository('SLUsersBundle:Stclass')->find($class);
                $course->setClass($class);
                $em->persist($course);
                $em->flush();
            }
            return $this->redirectToRoute('diary');
        }
        return $this->render(
            'SLUsersBundle:Users/Teacher:addcourse.html.twig',
            array('form' => $form->createView(),'inters'=>$inter)
        );
    }

    public function deleteAction($courseid)
    {
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('SLUsersBundle:Course')->find($courseid);
        $em->remove($course);
        $em->flush();

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function editAction($courseid, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find( $this->getUser()->getId());
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        $course = $em->getRepository('SLUsersBundle:Course')->find($courseid);
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,$course,array(
                'action' => $this->generateUrl('edit_course',array('courseid'=>$courseid))));
        $form=$formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $course->setTitle($request->request->get('title'));
            $course->setDescription($request->request->get('description'));
            $em->persist($course);
            $em->flush();
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $template = $this->render('SLUsersBundle:Users/Teacher:editcourse.html.twig', array('form' => $form->createView(),'inters'=>$inter,'course'=>$course))->getContent();
        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function publishAction($idcourse){
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('SLUsersBundle:Course')->find($idcourse);
        $course->setDone(true);
        $em->persist($course);
        $em->flush();
        return $this->redirect($_SERVER['HTTP_REFERER']);

    }

    public function unpublishAction($idcourse){
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('SLUsersBundle:Course')->find($idcourse);
        $course->setDone(false);
        $em->persist($course);
        $em->flush();
        return $this->redirect($_SERVER['HTTP_REFERER']);

    }
}
