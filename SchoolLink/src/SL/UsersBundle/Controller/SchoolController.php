<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\School;
use SL\UsersBundle\Form\SchoolEditForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class SchoolController extends Controller
{

    public function showAction()
    {
        $user= $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $school = $em->getRepository('SLUsersBundle:School')->find($user->getSchool()->getId());

        return $this->render('SLUsersBundle:Users/Admin:showschool.html.twig', array(
            'school' => $school,
        ));
    }

    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $school = $em->getRepository('SLUsersBundle:School')->find($this->getUser()->getSchool()->getId());
        $pic=$school->getSchoollogo();
        $editForm = $this->createForm(SchoolEditForm::class,$school, array(
            'action' => $this->generateUrl('edit_school')));

        $editForm->handleRequest($request);

        if ( $editForm->isValid()) {
            if ($editForm["schoollogo"]->getData() != null) {
                $file = $school->getSchoollogo();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                $this->getParameter('logo_directory'),
                $fileName
            );
            $school->setSchoollogo($fileName);
        }else{
            $school->setSchoollogo($pic);
        }
            $editForm->getData();
            $em->persist($school);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }

        return $this->render('SLUsersBundle:Users:Admin/editschool.html.twig', [
            'school' => $school,
            'form' => $editForm->createView()
        ]);
    }

}
