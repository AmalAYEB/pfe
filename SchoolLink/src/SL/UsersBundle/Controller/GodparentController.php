<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\Godparent;
use SL\UsersBundle\Entity\StudentGodparent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SL\UsersBundle\Form\GodparentRegistrationForm;
use Symfony\Component\Security\Core\Role\SwitchUserRole;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;


class GodparentController extends Controller
{
    public function viewAction($parentid){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $parent = $em->getRepository('SLUsersBundle:Godparent')->find($parentid);

        $response = new Response();
        $response->setContent(json_encode(array('parent' => $serializer->serialize($parent, 'json'))));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function listAction(){
        $em = $this->getDoctrine()->getManager();
        $parents = $em->getRepository('SLUsersBundle:Godparent')->findBy(array('gdschool' => $this->getUser()->getSchool()), null, null, null);
        return $this->render('SLUsersBundle:Users/Admin:listparents.html.twig', array(
            'parents' => $parents,
        ));
    }

    public function addAction(Request $request, $studentid)
    {
        $godparent = new Godparent();
        $Relation = new StudentGodparent();
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('SLUsersBundle:Student')->find($studentid);
        $form = $this->createForm(GodparentRegistrationForm::class, $godparent, array(
            'action' => $this->generateUrl('add_godparent',array('studentid'=>$studentid))));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form["picture"]->getData() != null) {
                $file = $godparent->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
            $godparent->setPicture($fileName);
             }
            $godparent->setGdschool($this->getUser()->getSchool());
            $godparent->setLastActivity(new \DateTime('now'));
            $Relation->setRelationtype($request->get('relation'));
            $Relation->setStudent($student);
            $Relation->setGodparent($godparent);
            $em->persist($godparent);
            $em->persist($Relation);
            $em->flush();
            return $this->redirectToRoute('edit_student',array('studentid' => $studentid, 'page'=> 'admin'));
        }
        return $this->render(
            'SLUsersBundle:Users/Godparent:registergodparent.html.twig',
            array('form' => $form->createView())
        );
    }

    public function showAction($studentid){
        $em = $this->getDoctrine()->getManager();
        $student =$em->getRepository('SLUsersBundle:Student')->find($studentid);
        $rel = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('student' => $student), null, null, null);
        $godparents=$em->getRepository('SLUsersBundle:Godparent')->findBy(array('gdschool' => $this->getUser()->getSchool()), null, null, null);
        return $this->render('SLUsersBundle:Users/Admin:studentgodparents.html.twig', array(
            'rels' => $rel,'godparents'=>$godparents, 'student' => $student
        ));
    }

    public function editAction(Request $request,$parentid,$page)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $parent = $em->getRepository('SLUsersBundle:Godparent')->find($parentid);
        $pic=$parent->getPicture();
        $editForm = $this->createForm(GodparentRegistrationForm::class,$parent, array(
            'action' => $this->generateUrl('edit_parent', array('parentid' => $parentid,'page'=>$page))));

        $editForm->handleRequest($request);

        if ( $editForm->isValid()) {
            if ($editForm["picture"]->getData() != null) {
                $file = $parent->getPicture();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('users_directory'),
                    $fileName
                );
                $parent->setPicture($fileName);
            }else{
                $parent->setPicture($pic);
            }
            $editForm->getData();
            $em->persist($parent);
            $em->flush();
            if($page=='parent'){
                return $this->redirect($_SERVER['HTTP_REFERER']);
            }
        }
        if($page=='parent') {
            return $this->render('SLUsersBundle:Users:Godparent/editparent.html.twig', [
                'parent' => $parent,
                'form' => $editForm->createView()
            ]);
        }else{
            return $this->render('SLUsersBundle:Users/Admin:editparent.html.twig', [
                'parent' => $parent,
                'form' => $editForm->createView()
            ]);
        }
    }

    public function deleteAction($parentid)
    {
        $em = $this->getDoctrine()->getManager();
        $godparent = $em->getRepository('SLUsersBundle:Godparent')->find($parentid);
        $rels = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('godparent' => $godparent), null, null, null);
        $reciev = $em->getRepository('SLUsersBundle:Message')->findBy(array('receiver' => $godparent), null, null, null);
        $send = $em->getRepository('SLUsersBundle:Message')->findBy(array('sender' => $godparent), null, null, null);
        foreach ($rels as $rel ) {
            $em->remove($rel);
        }
        if($reciev!=null){
            foreach ($reciev as $r) {
                $em->remove($r);
            }
        }
        if($send!=null){
            foreach ($send as $s) {
                $em->remove($s);
            }
        }

        $em->remove($godparent);
        $em->flush();

        return $this->render('SLUsersBundle:Users/Admin:parentsAdmin.html.twig');
    }

    public function studentsAction ($sender){
        $em = $this->getDoctrine()->getManager();

        if($sender=='student') {
            $authChecker = $this->get('security.authorization_checker');
            $tokenStorage = $this->get('security.token_storage');

            if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN')) {
                foreach ($tokenStorage->getToken()->getRoles() as $role) {
                    if ($role instanceof SwitchUserRole) {
                        $gp = $role->getSource()->getUser();
                        break;
                    }
                }
            }
            $godparent = $em->getRepository('SLUsersBundle:Godparent')->find($gp->getId());

        }else {
            $godparent = $em->getRepository('SLUsersBundle:Godparent')->find($this->getUser()->getId());
        }
        $relations = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('godparent' => $godparent), null, null, null);
        return $this->render('SLUsersBundle:Users/Godparent:studentsprofils.html.twig', array(
            'relations' => $relations
        ));
    }

    public function menuAction()
    {

        $list = array(
            array('title' => "Accueil", 'path' => 'homepage','icon'=>"fa fa-home sidebar-nav-icon"),
            array('title' => "Enseignants", 'path' => 'parenttechers','icon'=>"fa fa-graduation-cap sidebar-nav-icon"),
        );

        return $this->render('SLUsersBundle:Users:menu.html.twig', array(
            'list' => $list
        ));
    }


    public function chatlistAction(){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();

        $authChecker = $this->get('security.authorization_checker');
        $tokenStorage = $this->get('security.token_storage');
        $rol=$this->getUser()->getRoles();
        if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN') && $rol[0]=="ROLE_STUDENT") {
            foreach ($tokenStorage->getToken()->getRoles() as $role) {
                if ($role instanceof SwitchUserRole) {
                    $gp = $role->getSource()->getUser();
                    break;
                }
            }

                $relations = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('godparent' => $gp), null, null, null);
                $godparent = $em->getRepository('SLUsersBundle:Godparent')->find($gp->getId());
                $admin = $em->getRepository('SLUsersBundle:Admin')->findBy(array('school' => $godparent->getGdschool()), null, null, null);

        }else {
            $relations = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('godparent' => $this->getUser()), null, null, null);
            $admin=$em->getRepository('SLUsersBundle:Admin')->findBy(array('school' => $this->getUser()->getGdschool()),null ,null, null);
        }
        $inters=array(NULL);
        $i=0;
        foreach($relations as $relation) {
            $inters[$i] = $em->getRepository('SLUsersBundle:Intermediate')->findBy(array('stclass' => $relation->getStudent()->getStclass()), null, null, null);
            $i++;
        }
        $response = new Response();
        $response->setContent(json_encode(array(
            'inters' => $serializer->serialize($inters, 'json'),
            'admin' => $serializer->serialize($admin, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function teachersAction(){
        $em = $this->getDoctrine()->getManager();

        $authChecker = $this->get('security.authorization_checker');
        $tokenStorage = $this->get('security.token_storage');
        $rol=$this->getUser()->getRoles();
        if ($authChecker->isGranted('ROLE_PREVIOUS_ADMIN') && $rol[0]=="ROLE_STUDENT") {
            foreach ($tokenStorage->getToken()->getRoles() as $role) {
                if ($role instanceof SwitchUserRole) {
                    $gp = $role->getSource()->getUser();
                    break;
                }
            }
            $relations = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('godparent' => $gp), null, null, null);
        }else {
            $relations = $em->getRepository('SLUsersBundle:StudentGodparent')->findBy(array('godparent' => $this->getUser()), null, null, null);
        }
        $inters=array(NULL);
        $i=0;
        foreach($relations as $relation) {
            $inters[$i] = $em->getRepository('SLUsersBundle:Intermediate')->findBy(array('stclass' => $relation->getStudent()->getStclass()), null, null, null);
            $i++;
        }

        return $this->render('SLUsersBundle:Users/Godparent:teachersParent.html.twig',array('inters'=>$inters));
    }
}
