<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\Stclass;
use SL\UsersBundle\Form\StclassForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StclassController extends Controller
{
    public function showAction($levelid)
    {
        $em = $this->getDoctrine()->getManager();
        $level = $em->getRepository('SLUsersBundle:Level')->find($levelid);
        $classes = $em->getRepository('SLUsersBundle:Stclass')->findBy(array('level' =>$level ),null, null, null);
        $template = $this->render('SLUsersBundle:Users/Admin:showclass.html.twig',array('levelid'=>$levelid, 'classes' => $classes))->getContent();
        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function addAction(Request $request,$levelid)
    {
        $class = new Stclass();
        $em = $this->getDoctrine()->getManager();
        $lev = $em->getRepository('SLUsersBundle:Level')->find($levelid);
        $class->setLevel($lev);
        $form = $this->createForm(StclassForm::class, $class, array(
            'action' => $this->generateUrl('add_class',array('levelid'=>$levelid))));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($class);
            $em->flush();
            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $formcl= $this->render('SLUsersBundle:Users/Admin:addclass.html.twig', array('form' => $form->createView()))->getContent();
        $json = json_encode($formcl);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function deleteAction($classid)
    {
        $em = $this->getDoctrine()->getManager();
        $class = $em->getRepository('SLUsersBundle:Stclass')->find($classid);
        $student = $em->getRepository('SLUsersBundle:Student')->findBy(array('stclass' => $class), null, null, null);
        if($student==null) {
            $em->remove($class);
            $em->flush();
        }

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function editAction(Request $request,$classid)
    {
        $em = $this->getDoctrine()->getManager();
        $class = $em->getRepository('SLUsersBundle:Stclass')->find($classid);
        $form = $this->createForm(StclassForm::class,$class, array(
            'action' => $this->generateUrl('edit_class',array('classid'=>$classid))));
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            $form->getData();
            $em->persist($class);
            $em->flush();

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $template=$this->render('SLUsersBundle:Users:Admin/addclass.html.twig', ['class' => $class, 'form' => $form->createView()])->getContent();
        $json = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function viewAction($classid)
    {
        $em = $this->getDoctrine()->getManager();
        $class = $em->getRepository('SLUsersBundle:Stclass')->find($classid);
        return $this->render('SLUsersBundle:Users/Admin:viewclassAdmin.html.twig',array('class'=>$class));
    }

    public function showallAction($classid){
        $em = $this->getDoctrine()->getManager();
        $class=$em->getRepository('SLUsersBundle:Stclass')->find($classid);
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), null, null, null);
        $classes = $em->getRepository('SLUsersBundle:Stclass')->findAll();
        return $this->render('SLUsersBundle:Users/Admin:passstudents.html.twig',array('class'=>$class,'levels'=>$levels,'classes'=>$classes));
    }

    }
