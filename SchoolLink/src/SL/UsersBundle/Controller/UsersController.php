<?php

namespace SL\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SL\UsersBundle\Form\LoginForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FormType;


class UsersController extends Controller
{
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last login entered by the user
        $lastLogin = $authenticationUtils->getLastUsername();
        $form = $this->createForm(LoginForm::class, [
            'login' => $lastLogin,
        ]);

        return $this->render('SLUsersBundle:Users:login.html.twig', array(
            'form' => $form->createView(),
            'error'         => $error,
        ));
    }

    public function logoutAction()
    {
        throw new \Exception('Cela ne devrait pas être atteint!');
    }

    public function indexAction(){
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            return $this->render('SLUsersBundle:Users/Admin:hompageAdmin.html.twig');
        }elseif ($this->get('security.authorization_checker')->isGranted('ROLE_TEACHER')){
            return $this->render('SLUsersBundle:Users/Teacher:hompageTeacher.html.twig');
        }elseif ($this->get('security.authorization_checker')->isGranted('ROLE_GODPARENT')){
            return $this->render('SLUsersBundle:Users/Godparent:homepageGodparent.html.twig');
        }elseif ($this->get('security.authorization_checker')->isGranted('ROLE_STUDENT')){
            return $this->render('SLUsersBundle:Users/Student:hompageStudent.html.twig');
        }else{
            throw $this->createAccessDeniedException('GET OUT!');
        }
    }

    public function forgotpasswordAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $formBuilder = $this->get('form.factory')->createBuilder(FormType::class,array(
            'action' => $this->generateUrl('forgot_password')));
        $form=$formBuilder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tel=$_POST['tel'];
            $user=$em->getRepository('SLUsersBundle:User')->findOneBy(array('phone' => $tel));
        }
        return $this->render('SLUsersBundle:Users:forgotpassword.html.twig');
    }
}
