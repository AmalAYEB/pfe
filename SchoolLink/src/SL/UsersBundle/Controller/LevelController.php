<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Form\LevelForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SL\UsersBundle\Entity\Level;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class LevelController extends Controller
{
    public function showAction(){
        $em = $this->getDoctrine()->getManager();
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), null, null, null);
       return $this->render('SLUsersBundle:Users/Admin:showlevel.html.twig',array(
            'levels' => $levels,
        ));
    }

    public function showsubAction(){
        $em = $this->getDoctrine()->getManager();
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), null, null, null);
        return $this->render('SLUsersBundle:Users/Admin:showsubjects.html.twig',array(
            'levels' => $levels,
        ));
    }

    public function listAction(){
        $em = $this->getDoctrine()->getManager();
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), array('id' => 'desc'), null, null);
        return $this->render('SLUsersBundle:Users/Admin:listlevel.html.twig',array(
            'levels' => $levels,
        ));
    }

    public function addAction(Request $request)
    {
        $level = new Level();
        $form = $this->createForm(LevelForm::class, $level,array(
            'action' => $this->generateUrl('add_level')));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $level->setSchoollevel($this->getUser()->getSchool());
            $em = $this->getDoctrine()->getManager();
            $em->persist($level);
            $em->flush();
            return $this->redirectToRoute('classes');
            $this->addFlash('notice', 'Success');
        }else{
            $this->addFlash('notice', 'Not Success');
        }
        return $this->render(
            'SLUsersBundle:Users/Admin:addlevel.html.twig',
            array('form' => $form->createView())
        );
    }

    public function deleteAction($levelid)
    {
        $em = $this->getDoctrine()->getManager();
        $level = $em->getRepository('SLUsersBundle:Level')->find($levelid);
        $classes = $em->getRepository('SLUsersBundle:Stclass')->findBy(array('level' => $level), null, null, null);
        if($classes==null) {
            $em->remove($level);
            $em->flush();
        }

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function editAction($levelid,$leveltitle)
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $level = $em->getRepository('SLUsersBundle:Level')->find($levelid);
        $level->setLeveltitle($leveltitle);
        $em->persist($level);
        $em->flush();
        $response = new Response();
        $response->setContent(json_encode(array(
            'level' => $serializer->serialize($level, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function selectlistAction(){
        $em = $this->getDoctrine()->getManager();
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), array('id' => 'desc'), null, null);
        return $this->render('SLUsersBundle:Users/Admin:showselectlist.html.twig',array(
            'levels' => $levels,
        ));
    }

}
