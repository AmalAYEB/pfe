<?php

namespace SL\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SL\UsersBundle\Entity\Subject;
use SL\UsersBundle\Form\SubjectForm;

class SubjectController extends Controller
{

    public function showAction($levelid)
    {
        $em = $this->getDoctrine()->getManager();
        $level = $em->getRepository('SLUsersBundle:Level')->find($levelid);
        $subjects = $em->getRepository('SLUsersBundle:Subject')->findBy(array('level' =>$level ),null, null, null);
        return $this->render('SLUsersBundle:Users/Admin:listsubjects.html.twig',array(
            'subjects' => $subjects,
        ));
    }

    public function addAction(Request $request,$levelid=0)
    {

        $subject = new Subject();
        $em = $this->getDoctrine()->getManager();
        $lev = $em->getRepository('SLUsersBundle:Level')->find($levelid);
        $subject->setLevel($lev);
        $subject->setCreation(new \DateTime('now'));
        $form = $this->createForm(SubjectForm::class, $subject, array(
            'action' => $this->generateUrl('add_subject',array('levelid'=>$levelid))));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($subject);
            $em->flush();
            return $this->redirectToRoute('subjects');
        }
        return $this->render(
            'SLUsersBundle:Users/Admin:addsubject.html.twig',
            array('form' => $form->createView())
        );
    }

    public function deleteAction($subjectid)
    {
        $em = $this->getDoctrine()->getManager();
        $subject = $em->getRepository('SLUsersBundle:Subject')->find($subjectid);
        $em->remove($subject);
        $em->flush();

        return $this->render('SLUsersBundle:Users/Admin:subjectsAdmin.html.twig');
    }

    public function editAction(Request $request,$subjectid)
    {
        $em = $this->getDoctrine()->getManager();
        $subject = $em->getRepository('SLUsersBundle:Subject')->find($subjectid);
        $form = $this->createForm(SubjectForm::class,$subject, array(
            'action' => $this->generateUrl('edit_subject',array('subjectid'=>$subjectid))));
        $form->handleRequest($request);

        if ($request->isMethod('POST') && $form->isValid()) {
            $form->getData();
            $em->persist($subject);
            $em->flush();

            return $this->redirectToRoute('subjects');
        }

        return $this->render('SLUsersBundle:Users:Admin/addsubject.html.twig', [
            'subject' => $subject,
            'form' => $form->createView()
        ]);
    }
}
