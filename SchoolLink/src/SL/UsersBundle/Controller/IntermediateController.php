<?php

namespace SL\UsersBundle\Controller;

use SL\UsersBundle\Entity\Intermediate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class IntermediateController extends Controller
{

    public function assignmentAction(Request $request,$teacherid){
        $em = $this->getDoctrine()->getEntityManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find($teacherid);
        $levels = $em->getRepository('SLUsersBundle:Level')->findBy(array('schoollevel' => $this->getUser()->getSchool()), null, null, null);
        $classes = $em->getRepository('SLUsersBundle:Stclass')->findAll();
        $subjects = $em->getRepository('SLUsersBundle:Subject')->findAll();
        $inters = $em->getRepository('SLUsersBundle:Intermediate')->findAll();
        $taecherint = $em->getRepository('SLUsersBundle:Intermediate')->findBy(array('teacher' => $teacher),null ,null, null);
        return $this->render('SLUsersBundle:Users/Admin:teacherAssignment.html.twig', array(
            'levels' => $levels,
            'classes' => $classes,
            'subjects' => $subjects,
            'inters' => $inters,
            'taecherints' => $taecherint,
            'teacher' => $teacher
        ));
    }

    public function addAction($idteacher,$idsubject,$idclass){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getEntityManager();
        $teacher=$em->getRepository('SLUsersBundle:Teacher')->find($idteacher);
        $subject=$em->getRepository('SLUsersBundle:Subject')->find($idsubject);
        $class=$em->getRepository('SLUsersBundle:Stclass')->find($idclass);
        $inter=new Intermediate();
        $inter->setTeacher($teacher);
        $inter->setSubject($subject);
        $inter->setStclass($class);
        $em->persist($inter);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array(
            'inter' => $serializer->serialize($inter, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function deleteAction($idinter){
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getEntityManager();
        $inter=$em->getRepository('SLUsersBundle:Intermediate')->find($idinter);
        $em->remove($inter);
        $em->flush();

        $response = new Response();
        $response->setContent(json_encode(array(
            'inter' => $serializer->serialize($inter, 'json'),
        )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
