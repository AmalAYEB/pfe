<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Godparent
 *
 * @ORM\Table(name="godparent")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\GodparentRepository")
 */
class Godparent extends User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cingodparent", type="string", length=255)
     */
    private $cingodparent;

    /**
     * @var string
     *
     * @ORM\Column(name="occupation", type="string", length=255)
     */
    private $occupation;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\School")
     * @ORM\JoinColumn(nullable=true)
     */
    private $gdschool;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cingodparent
     *
     * @param string $cingodparent
     *
     * @return Godparent
     */
    public function setCingodparent($cingodparent)
    {
        $this->cingodparent = $cingodparent;

        return $this;
    }

    /**
     * Get cingodparent
     *
     * @return string
     */
    public function getCingodparent()
    {
        return $this->cingodparent;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     *
     * @return Godparent
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * @param \DateTime $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param string $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = $this->roles;
        if (!in_array('ROLE_GODPARENT', $roles)) {
            $roles[] = 'ROLE_GODPARENT';
        }
        return $roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles( array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * Get occupation
     *
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set gdschool
     *
     * @param \SL\UsersBundle\Entity\School $gdschool
     *
     * @return Godparent
     */
    public function setGdschool(\SL\UsersBundle\Entity\School $gdschool = null)
    {
        $this->gdschool = $gdschool;

        return $this;
    }

    /**
     * Get gdschool
     *
     * @return \SL\UsersBundle\Entity\School
     */
    public function getGdschool()
    {
        return $this->gdschool;
    }
}
