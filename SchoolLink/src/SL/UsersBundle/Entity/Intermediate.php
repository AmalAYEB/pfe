<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Intermediate
 *
 * @ORM\Table(name="intermediate")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\IntermediateRepository")
 */
class Intermediate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Teacher")
     * @ORM\JoinColumn(nullable=false)
     */
    private $teacher;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Subject")
     * @ORM\JoinColumn(nullable=false)
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Stclass")
     * @ORM\JoinColumn(nullable=false)
     */
    private $stclass;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teacher
     *
     * @param \SL\UsersBundle\Entity\Teacher $teacher
     *
     * @return Intermediate
     */
    public function setTeacher(\SL\UsersBundle\Entity\Teacher $teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \SL\UsersBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set subject
     *
     * @param \SL\UsersBundle\Entity\Subject $subject
     *
     * @return Intermediate
     */
    public function setSubject(\SL\UsersBundle\Entity\Subject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \SL\UsersBundle\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set stclass
     *
     * @param \SL\UsersBundle\Entity\Stclass $stclass
     *
     * @return Intermediate
     */
    public function setStclass(\SL\UsersBundle\Entity\Stclass $stclass)
    {
        $this->stclass = $stclass;

        return $this;
    }

    /**
     * Get stclass
     *
     * @return \SL\UsersBundle\Entity\Stclass
     */
    public function getStclass()
    {
        return $this->stclass;
    }
}
