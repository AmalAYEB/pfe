<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SL\UsersBundle\Repository\UserRepository;

/**
 * Student
 *
 * @ORM\Table(name="student")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\StudentRepository")
 */
class Student extends User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date")
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="birthplace", type="string", length=255)
     */
    private $birthplace;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Stclass")
     * @ORM\JoinColumn(nullable=true)
     */
    private $stclass;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\School")
     * @ORM\JoinColumn(nullable=true)
     */
    private $stschool;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Student
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set birthplace
     *
     * @param string $birthplace
     *
     * @return Student
     */
    public function setBirthplace($birthplace)
    {
        $this->birthplace = $birthplace;

        return $this;
    }

    /**
     * Get birthplace
     *
     * @return string
     */
    public function getBirthplace()
    {
        return $this->birthplace;
    }


    public function getRoles()
    {
        $roles = $this->roles;
        if (!in_array('ROLE_STUDENT', $roles)) {
            $roles[] = 'ROLE_STUDENT';
        }
        return $roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }


    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


    public function getSex()
    {
        return $this->sex;
    }

    public function setSex($sex)
    {
        $this->sex = $sex;
    }


    public function getPicture()
    {
        return $this->picture;
    }


    public function setPicture($picture)
    {
        $this->picture = $picture;
    }


    public function getPhone()
    {
        return $this->phone;
    }


    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


    public function getAddress()
    {
        return $this->address;
    }


    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getLogin()
    {
        return $this->login;
    }


    public function setLogin($login)
    {
        $this->login = $login;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Set stclass
     *
     * @param \SL\UsersBundle\Entity\Stclass $stclass
     *
     * @return Student
     */
    public function setStclass(\SL\UsersBundle\Entity\Stclass $stclass)
    {
        $this->stclass = $stclass;

        return $this;
    }

    /**
     * Get stclass
     *
     * @return \SL\UsersBundle\Entity\Stclass
     */
    public function getStclass()
    {
        return $this->stclass;
    }




    /**
     * Set stschool
     *
     * @param \SL\UsersBundle\Entity\School $stschool
     *
     * @return Student
     */
    public function setStschool(\SL\UsersBundle\Entity\School $stschool = null)
    {
        $this->stschool = $stschool;

        return $this;
    }

    /**
     * Get stschool
     *
     * @return \SL\UsersBundle\Entity\School
     */
    public function getStschool()
    {
        return $this->stschool;
    }
}
