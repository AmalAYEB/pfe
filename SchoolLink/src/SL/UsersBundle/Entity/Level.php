<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Level
 *
 * @ORM\Table(name="level")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\LevelRepository")
 */
class Level
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="leveltitle", type="string", length=255)
     */
    private $leveltitle;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\School", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $schoollevel;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set leveltitle
     *
     * @param string $leveltitle
     *
     * @return Level
     */
    public function setLeveltitle($leveltitle)
    {
        $this->leveltitle = $leveltitle;

        return $this;
    }

    /**
     * Get leveltitle
     *
     * @return string
     */
    public function getLeveltitle()
    {
        return $this->leveltitle;
    }

    /**
     * Set schoollevel
     *
     * @param \SL\UsersBundle\Entity\School $schoollevel
     *
     * @return Level
     */
    public function setSchoollevel(\SL\UsersBundle\Entity\School $schoollevel)
    {
        $this->schoollevel = $schoollevel;

        return $this;
    }

    /**
     * Get schoollevel
     *
     * @return \SL\UsersBundle\Entity\School
     */
    public function getSchoollevel()
    {
        return $this->schoollevel;
    }
}
