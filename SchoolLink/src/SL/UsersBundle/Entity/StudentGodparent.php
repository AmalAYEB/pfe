<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StudentGodparent
 *
 * @ORM\Table(name="student_godparent")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\StudentGodparentRepository")
 */
class StudentGodparent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="relationtype", type="string", length=255)
     */
    private $relationtype;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Student")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Godparent")
     * @ORM\JoinColumn(nullable=false)
     */
    private $godparent;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set relationtype
     *
     * @param string $relationtype
     *
     * @return StudentGodparent
     */
    public function setRelationtype($relationtype)
    {
        $this->relationtype = $relationtype;

        return $this;
    }

    /**
     * Get relationtype
     *
     * @return string
     */
    public function getRelationtype()
    {
        return $this->relationtype;
    }

    /**
     * Set student
     *
     * @param \SL\UsersBundle\Entity\Student $student
     *
     * @return StudentGodparent
     */
    public function setStudent(\SL\UsersBundle\Entity\Student $student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \SL\UsersBundle\Entity\Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set godparent
     *
     * @param \SL\UsersBundle\Entity\Godparent $godparent
     *
     * @return StudentGodparent
     */
    public function setGodparent(\SL\UsersBundle\Entity\Godparent $godparent)
    {
        $this->godparent = $godparent;

        return $this;
    }

    /**
     * Get godparent
     *
     * @return \SL\UsersBundle\Entity\Godparent
     */
    public function getGodparent()
    {
        return $this->godparent;
    }
}
