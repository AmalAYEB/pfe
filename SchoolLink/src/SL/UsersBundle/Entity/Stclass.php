<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stclass
 *
 * @ORM\Table(name="stclass")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\StclassRepository")
 */
class Stclass
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Level")
     * @ORM\JoinColumn(nullable=false)
     */
    private $level;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Stclass
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set level
     *
     * @param \SL\UsersBundle\Entity\Level $level
     *
     * @return Stclass
     */
    public function setLevel(\SL\UsersBundle\Entity\Level $level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \SL\UsersBundle\Entity\Level
     */
    public function getLevel()
    {
        return $this->level;
    }

}
