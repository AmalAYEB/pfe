<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Teacher
 *
 * @ORM\Table(name="teacher")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\TeacherRepository")
 */
class Teacher extends User
{
    public function getFirstname()
    {
        return $this->firstname;
    }


    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * @param \DateTime $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    }

    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


    public function getSex()
    {
        return $this->sex;
    }

    public function setSex($sex)
    {
        $this->sex = $sex;
    }


    public function getPicture()
    {
        return $this->picture;
    }


    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getLogin()
    {
        return $this->login;
    }


    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        $roles = $this->roles;
        if (!in_array('ROLE_TEACHER', $roles)) {
            $roles[] = 'ROLE_TEACHER';
        }
        return $roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cinteacher", type="string", length=255)
     */
    private $cinteacher;


    /**
     * @var string
     *
     * @ORM\Column(name="cv", type="string", length=255)
     */
    private $cv;

    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\School")
     * @ORM\JoinColumn(nullable=true)
     */
    private $schoolid_teacher;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cinteacher
     *
     * @param string $cinteacher
     *
     * @return Teacher
     */
    public function setCinteacher($cinteacher)
    {
        $this->cinteacher = $cinteacher;

        return $this;
    }

    /**
     * Get cinteacher
     *
     * @return string
     */
    public function getCinteacher()
    {
        return $this->cinteacher;
    }


    /**
     * Set cv
     *
     * @param string $cv
     *
     * @return Teacher
     */
    public function setCv($cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get cv
     *
     * @return string
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Set schoolidTeacher
     *
     * @param \SL\UsersBundle\Entity\School $schoolidTeacher
     *
     * @return Teacher
     */
    public function setSchoolidTeacher(\SL\UsersBundle\Entity\School $schoolidTeacher)
    {
        $this->schoolid_teacher = $schoolidTeacher;

        return $this;
    }

    /**
     * Get schoolidTeacher
     *
     * @return \SL\UsersBundle\Entity\School
     */
    public function getSchoolidTeacher()
    {
        return $this->schoolid_teacher;
    }
}