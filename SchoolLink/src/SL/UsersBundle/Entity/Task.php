<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\TaskRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"task" = "Task", "activity" = "Activity", "course" = "Course","evaluation"="Evaluation"})
 */
class Task
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    protected $description;


    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Subject")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $subject;


    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Teacher")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $teacher;


    /**
     * @ORM\ManyToOne(targetEntity="SL\UsersBundle\Entity\Stclass")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $class;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set subject
     *
     * @param \SL\UsersBundle\Entity\Subject $subject
     *
     * @return Task
     */
    public function setSubject(\SL\UsersBundle\Entity\Subject $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \SL\UsersBundle\Entity\Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set teacher
     *
     * @param \SL\UsersBundle\Entity\Teacher $teacher
     *
     * @return Task
     */
    public function setTeacher(\SL\UsersBundle\Entity\Teacher $teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \SL\UsersBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set class
     *
     * @param \SL\UsersBundle\Entity\Stclass $class
     *
     * @return Task
     */
    public function setClass(\SL\UsersBundle\Entity\Stclass $class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \SL\UsersBundle\Entity\Stclass
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
