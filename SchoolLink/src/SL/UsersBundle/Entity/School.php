<?php

namespace SL\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * School
 *
 * @ORM\Table(name="school")
 * @ORM\Entity(repositoryClass="SL\UsersBundle\Repository\SchoolRepository")
 */
class School
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolname", type="string", length=255)
     */
    private $schoolname;

    /**
     * @var string
     *
     * @ORM\Column(name="schoollogo", type="string", length=255,nullable=true)
     */
    private $schoollogo;

    /**
     * @var string
     *
     * @ORM\Column(name="schooldirector", type="string", length=255)
     */
    private $schooldirector;

    /**
     * @var string
     *
     * @ORM\Column(name="schooladdress", type="text")
     */
    private $schooladdress;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolemail", type="string", length=255,nullable=true)
     */
    private $schoolemail;

    /**
     * @var string
     *
     * @ORM\Column(name="schoolphone", type="string", length=255)
     */
    private $schoolphone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="year", type="date")
     */
    private $Year;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set schoolname
     *
     * @param string $schoolname
     *
     * @return School
     */
    public function setSchoolname($schoolname)
    {
        $this->schoolname = $schoolname;

        return $this;
    }

    /**
     * Get schoolname
     *
     * @return string
     */
    public function getSchoolname()
    {
        return $this->schoolname;
    }

    /**
     * Set schoollogo
     *
     * @param string $schoollogo
     *
     * @return School
     */
    public function setSchoollogo($schoollogo)
    {
        $this->schoollogo = $schoollogo;

        return $this;
    }

    /**
     * Get schoollogo
     *
     * @return string
     */
    public function getSchoollogo()
    {
        return $this->schoollogo;
    }

    /**
     * Set schooldirector
     *
     * @param string $schooldirector
     *
     * @return School
     */
    public function setSchooldirector($schooldirector)
    {
        $this->schooldirector = $schooldirector;

        return $this;
    }

    /**
     * Get schooldirector
     *
     * @return string
     */
    public function getSchooldirector()
    {
        return $this->schooldirector;
    }

    /**
     * Set schooladdress
     *
     * @param string $schooladdress
     *
     * @return School
     */
    public function setSchooladdress($schooladdress)
    {
        $this->schooladdress = $schooladdress;

        return $this;
    }

    /**
     * Get schooladdress
     *
     * @return string
     */
    public function getSchooladdress()
    {
        return $this->schooladdress;
    }

    /**
     * Set schoolemail
     *
     * @param string $schoolemail
     *
     * @return School
     */
    public function setSchoolemail($schoolemail)
    {
        $this->schoolemail = $schoolemail;

        return $this;
    }

    /**
     * Get schoolemail
     *
     * @return string
     */
    public function getSchoolemail()
    {
        return $this->schoolemail;
    }

    /**
     * Set schoolphone
     *
     * @param string $schoolphone
     *
     * @return School
     */
    public function setSchoolphone($schoolphone)
    {
        $this->schoolphone = $schoolphone;

        return $this;
    }

    /**
     * Get schoolphone
     *
     * @return string
     */
    public function getSchoolphone()
    {
        return $this->schoolphone;
    }

    /**
     * Set year
     *
     * @param \DateTime $year
     *
     * @return School
     */
    public function setYear($year)
    {
        $this->Year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return \DateTime
     */
    public function getYear()
    {
        return $this->Year;
    }
}
