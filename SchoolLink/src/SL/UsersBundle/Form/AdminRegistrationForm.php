<?php

namespace SL\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use SL\UsersBundle\Entity\Admin;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class AdminRegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname', TextType::class,array('label' => false))
                ->add('name', TextType::class,array('label' => false))
                ->add('cinadmin', TextType::class,array('label' => false))
                ->add('sex', ChoiceType::class, array('choices' => array('Homme' => 'Homme', 'Femme' => 'Femme'), 'expanded' => true, 'multiple' => false,'label' => false))
                ->add('login',TextType::class,array('label' => false))
                ->add('plainPassword', RepeatedType::class,['type' => PasswordType::class])
                ->add('school', SchoolForm::class,array('label' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Admin::class
        ]);
    }
}
