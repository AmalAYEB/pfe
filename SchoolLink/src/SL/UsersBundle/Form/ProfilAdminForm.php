<?php

namespace SL\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProfilAdminForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('school')
                ->add('picture', FileType::class, array('data_class' => null,'label' => ' Modifiez la photo de profil','attr'=> array('class'=>'file_bt'),'required'=>false))
                ->add('address',TextareaType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Adresse')))
                ->add('phone',TextType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Téléphone')))
        ;
    }

    public function getParent()
    {
        return AdminRegistrationForm::class;
    }
}
