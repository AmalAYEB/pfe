<?php

namespace SL\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use SL\UsersBundle\Entity\School;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SchoolForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('schoolname',TextType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Nom de l\'école')))
            ->add('year',DateType::class, array('widget' => 'single_text', 'format' => 'yyyy-MM-dd','placeholder'=>'Année scolaire','label' => false,'attr'=> array('class'=>'form-control')))
                ->add('schooldirector',TextType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Directeur')))
                ->add('schooladdress',TextareaType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Adresse')))
                ->add('schoolphone',TextType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Téléphone')));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => School::class
        ]);
    }

    public function getBlockPrefix()
    {
        return 'sl_usersbundle_school';
    }


}
