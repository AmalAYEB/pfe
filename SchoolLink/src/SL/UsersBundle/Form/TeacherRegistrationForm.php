<?php

namespace SL\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use SL\UsersBundle\Entity\Teacher;

class TeacherRegistrationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname', TextType::class,array('label' => false))
            ->add('name', TextType::class,array('label' => false))
            ->add('cinteacher', TextType::class,array('label' => false))
            ->add('sex', ChoiceType::class, array('choices' => array('Homme' => 'Homme', 'Femme' => 'Femme'), 'expanded' => true, 'multiple' => false,'label' => false))
            ->add('login',TextType::class,array('label' => false))
            ->add('plainPassword', RepeatedType::class,['type' => PasswordType::class])
            ->add('cv',TextType::class, array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Lien vers le cv')))
            ->add('picture', FileType::class, array('data_class' => null,'label' => 'Photo de profil','attr'=> array('class'=>'file_bt'),'required'=>false))
            ->add('address',TextareaType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Adresse')))
            ->add('phone',TextType::class,array('label' => false,'attr'=> array('class'=>'form-control','placeholder'=>'Téléphone')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Teacher::class
        ]);
    }

}
