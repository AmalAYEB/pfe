<?php

namespace SL\UsersBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SchoolEditForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('schoolemail',EmailType::class,array('label' => false,'attr'=> array('class'=>'form-control'),'required'=>false))
            ->add('schoollogo', FileType::class, array('data_class' => null,'label' => ' Modifiez le logo','attr'=> array('class'=>'file_bt'),'required'=>false));
    }

    public function getParent()
    {
        return SchoolForm::class;
    }
}
