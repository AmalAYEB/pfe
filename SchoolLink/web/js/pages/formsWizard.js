/*
 *  Document   : formsWizard.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Wizard page
 */

var FormsWizard = function() {

    return {
        init: function() {
            /*
             *  Jquery Wizard, Check out more examples and documentation at http://www.thecodemine.org
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Set default wizard options */
            var wizardOptions = {
                focusFirstInput: true,
                disableUIStyles: true,
                inDuration: 0,
                outDuration: 0

            };


            /* Initialize Validation Wizard */
            $('#validation-wizard').formwizard({
                disableUIStyles: true,
                validationEnabled: true,
                validationOptions: {
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'span',
                    errorPlacement: function(error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function(e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function(e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        'admin_registration_form[firstname]': {
                            required: true,
                            minlength: 2
                        },
                        'admin_registration_form[name]': {
                            required: true,
                            minlength: 2
                        },
                        'admin_registration_form[cinadmin]': {
                            required: true,
                            minlength: 8,
                            maxlength: 8,
                            number: true
                        },
                        'admin_registration_form[login]': {
                            required: true,
                            minlength: 2
                        },
                        'admin_registration_form[plainPassword][first]': {
                            required: true,
                            minlength: 5
                        },
                        'admin_registration_form[sex]': {
                            required: true
                        },
                        'admin_registration_form[plainPassword][second]': {
                            required: true,
                            equalTo: '#admin_registration_form_plainPassword_first'
                        },
                        'example-validation-email': {
                            required: true,
                            email: true
                        },
                        'admin_registration_form[school][schoolname]': {
                            required: true,
                            minlength: 2
                        },
                        'admin_registration_form[school][schooldirector]': {
                            required: true,
                            minlength: 2
                        },
                        'admin_registration_form[school][year]': {
                            required: true,
                        },
                        'admin_registration_form[school][schooladdress]': {
                            required: false,
                        }
                    },
                    messages: {
                        'admin_registration_form[firstname]': {
                            required: 'Entrez votre prénom',
                            minlength: 'Votre prénom doit comporter au moins 2 caractères'
                        },
                        'admin_registration_form[name]': {
                            required: 'Entrez votre nom',
                            minlength: 'Votre nom doit comporter au moins 2 caractères'
                        },
                        'admin_registration_form[sex]': {
                            required: 'Choisissez votre sexe'
                        },
                        'admin_registration_form[cinadmin]': {
                            required: 'Entrez votre n° de CIN',
                            minlength: 'Votre n° de CIN doit comporter 8 chiffres',
                            maxlength: 'Votre n° de CIN doit comporter 8 chiffres'
                        },
                        'admin_registration_form[login]': {
                            required: 'Entrez votre login',
                            minlength: 'Votre login doit comporter au moins 2 caractères'
                        },
                        'admin_registration_form[school][schoolname]': {
                            required: 'Entrez le nom de l\'école',
                            minlength: 'Le nom de l\'école doit comporter au moins 2 caractères'
                        },
                        'admin_registration_form[school][schooldirector]': {
                            required: 'Entrez le nom de directeur',
                            minlength: 'Le nom de directeur doit comporter au moins 2 caractères'
                        },
                        'admin_registration_form[plainPassword][first]': {
                            required: 'Vérifiez votre mot de passe',
                            minlength: 'Votre mot de passe doit comporter au moins 5 caractères'
                        },
                        'admin_registration_form[plainPassword][second]': {
                            required: 'Confirmez votre mot de passe',
                            minlength: 'Votre mot de passe doit comporter au moins 5 caractères',
                            equalTo: 'Entrez le même mot de passe que ci-dessus'
                        },
                        'admin_registration_form[school][year]': {
                            required: 'Entrez l\'année scolaire'
                        },

                    }
                },
                inDuration: 0,
                outDuration: 0
            });
        }
    };
}();