/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

var FormsValidationparent = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validationparent').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-control').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'godparent_registration_form[firstname]': {
                        required: true,
                        minlength: 2
                    },
                    'godparent_registration_form[name]': {
                        required: true,
                        minlength: 2
                    },
                    'godparent_registration_form[login]': {
                        required: true,
                        minlength: 2
                    },
                    'godparent_registration_form[plainPassword][first]': {
                        required: true,
                        minlength: 5
                    },
                    'relation':{
                        required: true,
                    },
                    'godparent_registration_form[sex]': {
                        required: true
                    },
                    'godparent_registration_form[plainPassword][second]': {
                        required: true,
                        equalTo: '#godparent_registration_form_plainPassword_first'
                    },
                    'godparent_registration_form[cinteacher]': {
                        required: false,
                        minlength: 8,
                        maxlength: 8,
                        number: true
                    }
                },
                messages: {
                    'godparent_registration_form[firstname]': {
                        required: 'Entrez le prénom',
                        minlength: 'Le prénom doit comporter au moins 2 caractères'
                    },
                    'godparent_registration_form[cinteacher]': {
                        required: 'Entrez votre n° de CIN',
                        minlength: 'Le n° de CIN doit comporter 8 chiffres',
                        maxlength: 'Le n° de CIN doit comporter 8 chiffres'
                    },
                    'godparent_registration_form[name]': {
                        required: 'Entrez le nom',
                        minlength: 'Le nom doit comporter au moins 2 caractères'
                    },
                    'relation':{
                        required: 'Vous devez préciser la relation entre le parent et l\' élève',
                    },
                    'godparent_registration_form[sex]': {
                        required: 'Choisissez le sexe'
                    },
                    'godparent_registration_form[login]': {
                        required: 'Entrez le login',
                        minlength: 'Le login doit comporter au moins 2 caractères'
                    },
                    'godparent_registration_form[plainPassword][first]': {
                        required: 'Vérifiez le mot de passe',
                        minlength: 'Le mot de passe doit comporter au moins 5 caractères'
                    },
                    'godparent_registration_form[plainPassword][second]': {
                        required: 'Confirmez le mot de passe',
                        minlength: 'Le mot de passe doit comporter au moins 5 caractères',
                        equalTo: 'Entrez le même mot de passe que ci-dessus'
                    },

                }
            });
        }
    };
}();