/*
 *  Document   : formsValidation.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Validation page
 */

var FormsValidationadmin = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validationadmin').validate({
                errorClass: 'help-block animation-pullUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-control').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'profil_admin_form[firstname]': {
                        required: true,
                        minlength: 2
                    },
                    'profil_admin_form[name]': {
                        required: true,
                        minlength: 2
                    },
                    'profil_admin_form[login]': {
                        required: true,
                        minlength: 2
                    },
                    'profil_admin_form[plainPassword][first]': {
                        required: false,
                        minlength: 5
                    },
                    'profil_admin_form[sex]': {
                        required: true
                    },
                    'profil_admin_form[plainPassword][second]': {
                        required: false,
                        equalTo: '#profil_admin_form_plainPassword_first'
                    },
                    'profil_admin_form[cinteacher]': {
                        required: false,
                        minlength: 8,
                        maxlength: 8,
                        number: true
                    }
                },
                messages: {
                    'profil_admin_form[firstname]': {
                        required: 'Entrez le prénom',
                        minlength: 'Le prénom doit comporter au moins 2 caractères'
                    },
                    'profil_admin_form[cinteacher]': {
                        required: 'Entrez votre n° de CIN',
                        minlength: 'Le n° de CIN doit comporter 8 chiffres',
                        maxlength: 'Le n° de CIN doit comporter 8 chiffres'
                    },
                    'profil_admin_form[name]': {
                        required: 'Entrez le nom',
                        minlength: 'Le nom doit comporter au moins 2 caractères'
                    },
                    'profil_admin_form[sex]': {
                        required: 'Choisissez le sexe'
                    },
                    'profil_admin_form[login]': {
                        required: 'Entrez le login',
                        minlength: 'Le login doit comporter au moins 2 caractères'
                    },
                    'profil_admin_form[plainPassword][first]': {
                        required: 'Vérifiez le mot de passe',
                        minlength: 'Le mot de passe doit comporter au moins 5 caractères'
                    },
                    'profil_admin_form[plainPassword][second]': {
                        required: 'Confirmez le mot de passe',
                        minlength: 'Le mot de passe doit comporter au moins 5 caractères',
                        equalTo: 'Entrez le même mot de passe que ci-dessus'
                    },

                }
            });
        }
    };
}();