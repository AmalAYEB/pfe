
$('.nav-classes li a').click(function() {
    pagination();
    $('.subjects-select').change(function() {
    pagination();
    console.log("eeee");
});

});
    function pagination() {
        $('#pages').hide();
        var totalRows = $('.task-list').find('li').length;
        var recordPerPage = 6;
        var totalPages = Math.ceil(totalRows / recordPerPage);
        var pagnumbers = ' ';
        if (totalRows > recordPerPage) {
            $('#pages').show();
        }
        for (i = 1; i <= totalPages; i++) {
            pagnumbers += '<div class="pageNumber">' + i + '</div>';
        }
        $('#prev').html('<div class="pagination-left"><i class="fa fa-chevron-left"></i></div>');
        $('#pages').html(pagnumbers);
        $('#next').html('<div class="pagination-right"><i class="fa fa-chevron-right"></i></div>');

        var totalPagenum = $("div.pageNumber").length;
        if (totalPagenum > 3) {
            $("div.pageNumber").hide();
            $("div.pagination-right").show();
            for (var n = 1; n <= 3; n++) {
                $("div.pageNumber:nth-child(" + n + ")").show();
            }
        }
        displayevent();

        $('.pagination-right').click(function () {
            if ($("div.selected:last").nextAll('div.pageNumber').length > 3) {
                $("div.selected").last().nextAll(':lt(3)').show();
                $("div.selected").hide();
                displayevent();
                //lastposevent();
                $("div.pagination-right").show();
                $("div.pagination-left").show();
            } else {
                console.log($("div.selected").last().text());
                $("div.selected").last().nextAll().show();
                $("div.selected").hide();
                displayevent();
                $("div.pagination-right").hide();
                $("div.pagination-left").show();
            }
        });

        $('.pagination-left').click(function () {
            if ($("div.selected:first").prevAll('div.pageNumber').length > 3) {
                $(".selected").first().prevAll(':lt(3)').show();
                $(".selected").hide();
                $("div.pagination-left").show();
                $("div.pagination-right").show();
                displayevent();
            } else {
                console.log($(".selected").first().prevAll().text());
                $(".selected").first().prevAll().show();
                $(".selected").hide();
                $("div.pagination-left").hide();
                $("div.pagination-right").show();
                displayevent();
            }
        });
        $("#pages div:first").addClass("focus");
        $('.task-list').find('li').hide();
        var tr = $('.task-list li');
        for (var i = 0; i <= recordPerPage - 1; i++) {
            $(tr[i]).show();
        }

        $('#pages div').click(function (event) {
            $('.task-list').find('li').hide();
            var nBegin = ($(this).text() - 1) * recordPerPage;
            var nEnd = $(this).text() * recordPerPage - 1;
            $('#pages div').removeClass('focus');
            $(this).addClass('focus');
            for (var i = nBegin; i <= nEnd; i++) {
                $(tr[i]).show();
            }
        });
    }
    function displayevent() {
        $("div.pageNumber").each(function () {
            if ($(this).css('display') === 'block') {
                $(this).addClass('selected');
            } else {
                $(this).removeClass('selected');
            }
        });
    }

