

    $('.elt').dblclick(function () {
        txt = $(this).text();
        $(this).html("<form><input class='form-control' value='" + txt + "' /></form>");
    });

    $('.elt').keyup(function (e) {
        if (e.keyCode == 13) {
            txt = $('.elt input').val();
            $(this).html(txt);
        }
    });
/**************     Active Menu   **************/

    var url = window.location;
    // Will only work if string in href matches with location
    $('.sidebar-nav a[href="'+ url +'"]').addClass('active');

    // Will also work for relative and absolute hrefs
    $('.sidebar-nav a').filter(function() {
        return this.href == url;
    }).addClass('active');


/**************     TesxtBook teacher    **************/

//Textbook colors
$('.sideways li a').click(function(){
$('.sizeesp').attr('id',this.className);
});
 //Disable subjects
$('sub').click(
function  () {

    if ($('sub').value !== "sub")
    {
        $('initial').disabled=true; }
});

//Add Task
$('.task-form').hide();
$('#task-title').focus(function () {
    $('.task-form').show();
});

//Task type filter
$('.task-repetition').hide();
$('.eval-date').hide();
$('.activity-date').hide();
$('.activity-date-limit').hide();
$("#type-task").change(function () {
    if ($("#type-task option:selected").val() == "activity") {
        $('.task-repetition').show();
        $('.activity-date').show();
        $('.eval-date').hide();

        $('#once').change(function(){
                $('.activity-date').show();
                $('.activity-date-limit').hide();
            }
        );

        $('.repetition-btn').change(function(){
            $('.activity-date').hide();
            $('.activity-date-limit').show();
            }
        );
    }else if ($("#type-task option:selected").val() == "evaluation"){
        $('.task-repetition').hide();
        $('.eval-date').show();
        $('.activity-date').hide();
        $('.activity-date-limit').hide();
    }
});

$('.task-action [type="reset"]').click(function() {
    $('.task-form').hide();
});







