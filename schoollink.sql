-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 16 Novembre 2017 à 16:37
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `schoollink`
--

-- --------------------------------------------------------

--
-- Structure de la table `intermediate`
--

CREATE TABLE `intermediate` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `stclass_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `intermediate`
--

INSERT INTO `intermediate` (`id`, `teacher_id`, `subject_id`, `stclass_id`) VALUES
(1, 23, 2, 8),
(3, 23, 4, 8),
(4, 23, 1, 5),
(5, 23, 4, 5),
(6, 36, 2, 5),
(7, 36, 3, 5),
(8, 36, 8, 5),
(9, 36, 1, 8),
(10, 36, 3, 8),
(11, 36, 8, 8);

-- --------------------------------------------------------

--
-- Structure de la table `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `leveltitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schoollevel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `level`
--

INSERT INTO `level` (`id`, `leveltitle`, `schoollevel_id`) VALUES
(1, 'Préparatoire', 1),
(3, 'Premier', 1),
(5, 'Deuxième', 1);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `publication_id` int(11) DEFAULT NULL,
  `images` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `media`
--

INSERT INTO `media` (`id`, `publication_id`, `images`, `type`, `alt`) VALUES
(1, 7, 'a:1:{i:0;s:37:"171dc97b96d6a9c0ba57c8dbe2bbad0b.jpeg";}', 'img', 'image');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `unread` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id`, `sender_id`, `receiver_id`, `content`, `date`, `unread`) VALUES
(2, 23, 1, 'Salut!', '2017-09-03 20:55:02', 1),
(3, 23, 1, 'cv?', '2017-09-03 20:57:26', 1),
(8, 23, 32, 'hello', '2017-09-04 01:16:07', 1),
(9, 23, 1, 'bien?', '2017-09-04 12:45:17', 1),
(10, 1, 23, 'Salut!!', '2017-09-04 14:52:14', 1),
(12, 1, 23, 'oui bien', '2017-09-04 21:42:21', 1),
(13, 23, 1, 'et toi?', '2017-09-05 13:00:49', 1),
(14, 32, 23, 'hi', '2017-09-29 19:21:03', 1),
(15, 23, 32, 'cv?', '2017-09-29 19:28:36', 1),
(16, 32, 23, 'oui bien ', '2017-09-29 20:48:53', 1),
(17, 32, 23, 'et vous ?', '2017-09-29 20:48:58', 1),
(22, 23, 1, 'À quelle heure sera la réunion?', '2017-10-14 20:19:24', 1),
(23, 32, 1, 'Bonsoir', '2017-10-14 20:22:41', 1),
(24, 32, 1, 'J\'espère que vous allez bien', '2017-10-14 20:23:15', 1),
(25, 1, 23, 'Je vais bien merci', '2017-10-14 20:27:50', 1),
(26, 1, 23, 'La  réunion est à 14h', '2017-10-14 20:28:17', 1),
(27, 1, 32, 'Bonsoir', '2017-10-14 20:34:12', 0),
(28, 23, 1, 'Merci', '2017-10-16 14:23:24', 1);

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `contents` longtext COLLATE utf8_unicode_ci NOT NULL,
  `datepub` datetime NOT NULL,
  `school_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `publication`
--

INSERT INTO `publication` (`id`, `creator_id`, `contents`, `datepub`, `school_id`) VALUES
(1, 1, 'La semaine des examens sera la semaine de 22 Mai 2017.', '2017-05-05 13:33:02', 1),
(4, 1, 'Notre école organise une voyage à Tabarka le dimanche 04 Mai 2017. Pour plus d\'informations appelez : 25 147 256', '2017-05-05 17:41:56', 1),
(5, 1, 'Mme Dorra Ferchichi sera absente le lundi 20 Octobre 2017.', '2017-05-14 19:48:46', 1),
(7, 1, 'Documentaire sur les pandas.', '2017-09-16 21:53:30', 1),
(8, 23, 'Une animation sera lieu le Lundi 25 Septembre 2017.', '2017-09-17 00:10:01', 1),
(9, 1, 'Nous avons organisé une visite au musée de Bardo le dimanche 22 Octobre 2017. Pour plus d\'information veuillez me contacter.', '2017-10-16 14:50:33', 1);

-- --------------------------------------------------------

--
-- Structure de la table `school`
--

CREATE TABLE `school` (
  `id` int(11) NOT NULL,
  `schoolname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schoollogo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schooldirector` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `schooladdress` longtext COLLATE utf8_unicode_ci NOT NULL,
  `schoolemail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schoolphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `school`
--

INSERT INTO `school` (`id`, `schoolname`, `schoollogo`, `schooldirector`, `schooladdress`, `schoolemail`, `schoolphone`, `year`) VALUES
(1, 'Saint Joseph', 'a1c228d388b670ddc795a0d7f3304c6d.png', 'Amal Ayeb', '13 Rue Taoufik El Hakim Manouba', 'SaintJoseph@yahoo.fr', '71582963', '2017-09-15');

-- --------------------------------------------------------

--
-- Structure de la table `stclass`
--

CREATE TABLE `stclass` (
  `id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `stclass`
--

INSERT INTO `stclass` (`id`, `level_id`, `name`) VALUES
(5, 3, '1 A'),
(6, 1, 'Prép A'),
(7, 1, 'Prép B'),
(8, 3, '1 B');

-- --------------------------------------------------------

--
-- Structure de la table `student_godparent`
--

CREATE TABLE `student_godparent` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `godparent_id` int(11) NOT NULL,
  `relationtype` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `student_godparent`
--

INSERT INTO `student_godparent` (`id`, `student_id`, `godparent_id`, `relationtype`) VALUES
(4, 31, 32, 'Oncle'),
(88, 9, 28, 'Mère'),
(116, 9, 30, 'Oncle'),
(117, 37, 35, 'Mère'),
(118, 38, 42, 'Père'),
(119, 39, 43, 'Soeur'),
(120, 40, 35, 'Tante'),
(121, 40, 44, 'Père'),
(123, 34, 42, 'Père'),
(124, 38, 32, 'Oncle'),
(125, 34, 32, 'Oncle'),
(126, 48, 52, 'Grand-mère');

-- --------------------------------------------------------

--
-- Structure de la table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coef` int(11) NOT NULL,
  `Creation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `subject`
--

INSERT INTO `subject` (`id`, `level_id`, `name`, `coef`, `Creation`) VALUES
(1, 3, 'Production écrite', 3, '2017-05-10'),
(2, 3, 'ايقاظ علمي', 2, '2017-05-10'),
(3, 3, 'تربية اسلامية', 1, '2017-05-10'),
(4, 3, 'Mathématiques', 2, '2017-05-10'),
(5, 3, 'خط واملاء', 2, '2017-05-10'),
(7, 3, 'Lecture', 1, '2017-05-10'),
(8, 3, 'عربية', 2, '2017-10-06');

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `done` tinyint(1) DEFAULT NULL,
  `discr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `repetition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end` date DEFAULT NULL,
  `creation` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`id`, `subject_id`, `teacher_id`, `class_id`, `title`, `done`, `discr`, `date`, `description`, `repetition`, `end`, `creation`) VALUES
(11, 4, 23, 8, 'Evaluation en mathématiques', NULL, 'evaluation', '2017-05-29', 'Le sujet de l\'évaluation est les tables de multiplication.', NULL, NULL, NULL),
(14, 4, 23, 5, 'Exercice 5 page 201', NULL, 'activity', '2017-05-31', '', 'onlyonce', NULL, NULL),
(18, 4, 23, 5, 'Révision en Mathématiques', NULL, 'activity', '2017-08-18', '', 'weekly', '2017-08-31', NULL),
(19, 1, 23, 5, 'Evaluation en Production écrite', NULL, 'evaluation', '2017-08-15', '', NULL, NULL, NULL),
(20, 4, 23, 8, 'Les opérations d\'addition.', 0, 'course', NULL, '', NULL, NULL, '2017-05-31'),
(22, 4, 23, 5, 'aaaaaaaaaa', NULL, 'evaluation', '2017-08-07', 'aaaaaaaaaaa', NULL, NULL, NULL),
(27, 1, 23, 5, 'jjjjj', NULL, 'evaluation', '2017-09-02', 'eeeeee', NULL, NULL, NULL),
(31, 4, 23, 5, 'llll', NULL, 'evaluation', '2017-09-08', 'llllll', NULL, NULL, NULL),
(56, 4, 23, 5, 'zzzzzzzzzzz', NULL, 'activity', '2017-09-02', '', 'onlyonce', NULL, NULL),
(58, 4, 23, 8, 'sssssss', NULL, 'activity', '2017-08-30', '', 'onlyonce', NULL, NULL),
(59, 4, 23, 8, 'yyyyyyyyy', 0, 'course', NULL, '', NULL, NULL, '2017-08-22'),
(60, 4, 23, 8, 'uuuuuuuuuuu', 0, 'course', NULL, '', NULL, NULL, '2017-08-22'),
(64, 4, 23, 5, 'uuuuuuuu', 0, 'course', NULL, '', NULL, NULL, '2017-08-23'),
(65, 1, 23, 5, 'uuuuuuuuu', 0, 'course', NULL, 'llllllllllllll', NULL, NULL, '2017-08-23'),
(66, 2, 23, 8, 'ploplop', 0, 'course', NULL, 'tttttttttt', NULL, NULL, '2017-08-23'),
(67, 2, 23, 8, 'oooooooooo', 0, 'course', NULL, 'ooooo', NULL, NULL, '2017-08-23'),
(68, 2, 23, 8, 'iiiiiiiiiiiiii', 0, 'course', NULL, 'olllllllllll', NULL, NULL, '2017-08-23'),
(69, 2, 23, 8, 'jjjjjjjjjjjjjjjjj', 0, 'course', NULL, '', NULL, NULL, '2017-08-23'),
(72, 4, 23, 8, 'ttttttt', 0, 'course', NULL, 'tttttttttt', NULL, NULL, '2017-08-23'),
(73, 4, 23, 5, 'ttttttt', 0, 'course', NULL, 'tttttttttt', NULL, NULL, '2017-08-23'),
(74, 4, 23, 5, 'Test en math', NULL, 'evaluation', '2017-09-29', 'table de 2, 3 et 4', NULL, NULL, NULL),
(75, 1, 23, 5, 'Evaluation en production', NULL, 'evaluation', '2017-09-26', 'Thème nature ', NULL, NULL, NULL),
(76, 4, 23, 5, 'Les opérations d\'addition.', 1, 'course', NULL, '', NULL, NULL, '2017-09-25'),
(77, 4, 23, 5, 'Les opérations de soustraction.', 0, 'course', NULL, '', NULL, NULL, '2017-09-25'),
(78, 1, 23, 5, 'Description de la nature', 1, 'course', NULL, 'Description de la plage et de la forêt.', NULL, NULL, '2017-09-25'),
(79, 1, 23, 5, 'La rédaction d’une carte postale ou d’une lettre de vacances.', 0, 'course', NULL, '', NULL, NULL, '2017-10-07'),
(80, 1, 23, 5, 'La rédaction d’un résumé', 1, 'course', NULL, 'Suite à la lecture d\'une conte.', NULL, NULL, '2017-10-07'),
(81, 1, 23, 5, 'La rédaction d’un dialogue.', 1, 'course', NULL, '', NULL, NULL, '2017-10-07'),
(82, 1, 23, 5, 'Description d\'une personne.', 1, 'course', NULL, 'Description de visage.', NULL, NULL, '2017-10-07'),
(83, 1, 23, 5, 'Description d\'une personne.', 1, 'course', NULL, 'Description du corps.', NULL, NULL, '2017-10-07'),
(84, 1, 23, 5, 'Description d\'une personne.', 1, 'course', NULL, 'Description du mouvement.', NULL, NULL, '2017-10-07'),
(85, 1, 23, 5, 'La rédaction d’une lettre.', 0, 'course', NULL, '', NULL, NULL, '2017-10-07'),
(86, 2, 36, 5, 'الهواء ضروري لحياة الإنسان والحيوان والنبات', 1, 'course', NULL, '', NULL, NULL, '2017-10-07'),
(87, 2, 36, 5, 'مكوّنات الهواء وخصائصه', 1, 'course', NULL, '', NULL, NULL, '2017-10-07'),
(88, 2, 36, 5, ' الاحتراق في الهواء وأهميّة الأكسجين في عمليّة الاحتراق', 1, 'course', NULL, '', NULL, NULL, '2017-10-07'),
(89, 1, 23, 5, 'Exercice 5 page 25.', NULL, 'activity', '2017-10-13', 'Vous devez écrire une paragraphe de 10 lignes', 'onlyonce', NULL, NULL),
(90, 2, 23, 8, 'التبادل الغازي في مستوى الرئتين', 1, 'course', NULL, '', NULL, NULL, '2017-10-13'),
(91, 2, 23, 8, 'الدم ينقل الغذاء والغازات', 1, 'course', NULL, '', NULL, NULL, '2017-10-13'),
(93, 4, 23, 8, 'Test', NULL, 'evaluation', '2017-10-24', 'Vous devez réviser les opérations de multiplication.', NULL, NULL, NULL),
(94, 4, 23, 8, 'Exercice 5 page 25', NULL, 'activity', '2017-10-26', '', 'onlyonce', NULL, NULL),
(97, 4, 23, 8, 'Les table de multiplication de 7, 8 et 9.', NULL, 'activity', '2017-10-27', '', 'onlyonce', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sex` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8_unicode_ci,
  `discr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cinadmin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cinteacher` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cingodparent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `school_id` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birthplace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stclass_id` int(11) DEFAULT NULL,
  `schoolid_teacher_id` int(11) DEFAULT NULL,
  `gdschool_id` int(11) DEFAULT NULL,
  `firstlog` int(11) DEFAULT NULL,
  `lastActivity` datetime NOT NULL,
  `stschool_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `name`, `sex`, `picture`, `phone`, `address`, `discr`, `cinadmin`, `cinteacher`, `cv`, `cingodparent`, `occupation`, `login`, `password`, `roles`, `school_id`, `birthday`, `birthplace`, `stclass_id`, `schoolid_teacher_id`, `gdschool_id`, `firstlog`, `lastActivity`, `stschool_id`) VALUES
(1, 'Amal', 'Ayeb', 'Femme', '091e55e9ecbf56e89d2566e6aeb8ce32.png', NULL, NULL, 'admin', '08123455', NULL, NULL, NULL, NULL, 'Admin', '$2y$13$Spf1X5bxT53owHjJ0cqrMujmRwu0JfqjEuu9vbEKIHVoh2rJ.JpRe', '[]', 1, NULL, NULL, 0, 0, NULL, 1, '2017-11-16 16:22:16', NULL),
(9, 'Dorra', 'Khammari', 'Fille', 'dbc069c0117d105630d17c6d3454da0e.ico', NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Dorra', 'testdor', '[]', NULL, '2011-12-16', 'Manouba', 5, 0, NULL, NULL, '2017-09-16 11:19:24', NULL),
(23, 'Dorra', 'Ferchichi', 'Femme', 'f2c89afe0347544ec9014f40bc303cfa.png', '25 589 654', '13 Ksar Said Manouba', 'teacher', NULL, NULL, NULL, NULL, NULL, 'Teacher', '$2y$13$QtDYjjvHbjijqaYFyXUnNOPUhPMDa02l20AjVJ2iW8tkakkltmJPi', '[]', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-11-16 16:04:51', NULL),
(28, 'Zina', 'Khammari', 'Femme', 'a18683b800a586eb611cb7e8fc610f1f.png', '25 147  852', '13 Rue Bourguiba Manouba', 'godparent', NULL, NULL, NULL, '08147596', 'Médecin', 'Zina', '$2y$13$RBzIFl5gM8lCM0i6zsNh7uW4T4cnxTRq6ks0e92F1VE8M9PHsKpka', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-09-30 18:14:55', NULL),
(30, 'Mohamed', 'Ferchichi', 'Homme', '6f9876064d9d42af71610b0ca9b69bbf.png', '21 520 789', NULL, 'godparent', NULL, NULL, NULL, NULL, NULL, 'Mohamedlog', '$2y$13$9UcL/Xh89REuAYgGPvzvauhzKb3EvDe6b9JN94JhypI4GOAaOs4e.', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-09-04 16:23:16', NULL),
(31, 'Karim', 'Ben Ali', 'Garçon', '36796f5e9d0e6d4c14ead02f29bf0f68.jpeg', NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Student', '$2y$13$8Da0bZIwX3d33mHOtedtC.STZCNXFfiBlPPcASjzWntNSdYcIMGaq', '[]', NULL, '2010-02-10', 'Manouba', 5, NULL, NULL, 1, '2017-11-16 16:08:47', 1),
(32, 'Ali', 'Ben Amor', 'Homme', 'b54cd15c133dd3b8e3f44262f437560a.png', '98 234 613', NULL, 'godparent', NULL, NULL, NULL, '12369854', NULL, 'Gp', '$2y$13$7Z.b2srL/J5R74D/PwsFs.XKURvFgPPAs13/zfzf44yTKR7mAYHPu', '[]', NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-11-16 16:08:38', NULL),
(34, 'Amina', 'Khiari', 'Fille', 'b0e39d2b6cdb74df5d01475edae43dbd.png', NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Amina2', '$2y$13$p7sjzs15c4u/T00qcaWJ8e9gftV6zlyzrdQbF1OqXpf7QOWYfaQK2', '[]', NULL, '2010-06-18', 'Tunis', 8, NULL, NULL, NULL, '2017-10-16 17:58:46', 1),
(35, 'Samira', 'Yaich', 'Femme', '45484841.png', NULL, NULL, 'godparent', NULL, NULL, NULL, '12547896', NULL, 'Samira21', '$2y$13$np.y1x7bD4WplPNpFjkiV.KG5GFIBRQd3lkI6kVWDrPBDtNRPkJ2S', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-09-03 15:15:12', NULL),
(36, 'Ameni', 'Ben Arfi', 'Femme', 'f01c7e4b3e0767ae146d52e6ae95657b.png', '22 158 856', '12 Rue Marseille Tunis', 'teacher', NULL, '08125147', NULL, NULL, NULL, 'Ameni', '$2y$13$XIqF/5HYliIPCCgmaqWuKe/mbhjpzj7Da8XXsRvKMsy1sciSRfHIK', '[]', NULL, NULL, NULL, NULL, 1, NULL, 1, '2017-10-07 00:40:55', NULL),
(37, 'Nassim', 'Ammar', 'Garçon', NULL, NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Nassim', '$2y$13$c0zhgj3ojQryJ4tA285oi.ac6EqKcUD3QV0UAQnbg9bL2GwTbAP3m', '[]', NULL, '2010-03-11', 'Tunis', 5, NULL, NULL, NULL, '2017-10-06 22:42:36', NULL),
(38, 'Amin', 'Ben Ali', 'Garçon', NULL, NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Amin', '$2y$13$/6LdbhiG5/HG49kOEkLV..6SilLXjUo6MrGvMqttXsUcX8RlF0/66', '[]', NULL, '2010-02-11', 'Manouba', 5, NULL, NULL, NULL, '2017-10-16 17:59:10', 1),
(39, 'Salma', 'Krimi', 'Fille', NULL, NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Salma', '$2y$13$Hu2/OWE8OiM3wnzDWGAAPeRS8H4NkNEDBcGCRRi0r4TWQKUlvHlOG', '[]', NULL, '2011-06-15', NULL, 5, NULL, NULL, NULL, '2017-10-06 22:51:31', NULL),
(40, 'Manel', 'Arfaoui', 'Fille', NULL, NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'Manel', '$2y$13$vBvPAPTGpXCYDBxkbBj7uuG98lX0aMlvuIkxwbzqFKDZuV/A65w6C', '[]', NULL, '2011-06-03', NULL, 5, NULL, NULL, NULL, '2017-10-06 22:52:30', NULL),
(42, 'Salim', 'Ben Ali', 'Homme', NULL, NULL, NULL, 'godparent', NULL, NULL, NULL, '78945612', NULL, 'Salim', '$2y$13$cWg48657SP451NJY9o/Jcuh23S13klull4FzL5PNOkk92h/MTJKMW', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-10-06 22:56:04', 1),
(43, 'Samira', 'Krimi', 'Femme', NULL, NULL, NULL, 'godparent', NULL, NULL, NULL, '12369857', NULL, 'Samira', '$2y$13$TGxQZXSNHqHMZ/QUIMlf7OsaPYpTWezsb7RneatY79H9fUSAzCoVG', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-10-06 22:59:19', NULL),
(44, 'Yassin', 'Arfaoui', 'Homme', NULL, NULL, NULL, 'godparent', NULL, NULL, NULL, '08457632', NULL, 'Yassin', '$2y$13$rVm3.mR3Vnxv7WDodvibjeo7xomqZ2fefc7.KuS80NZ8K3.NbuvrC', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-10-06 23:03:51', NULL),
(45, 'Samir', 'Ayari', 'Homme', NULL, '55 852 369', NULL, 'teacher', NULL, '14852963', NULL, NULL, NULL, 'Samir', '$2y$13$kUg1YwIwt7Rcv.jlenuaFuV6sq1Pkwrr/CCKWqkVw2puWyUQ9GL22', '[]', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-10-13 18:06:51', NULL),
(48, 'Aziza', 'Selmi', 'Fille', NULL, NULL, NULL, 'student', NULL, NULL, NULL, NULL, NULL, 'aziza', '$2y$13$qMMNrDUHyFQpvnv98FcXq.YMZzXXnYGIMH5PVJiIUgG9nqoUtbzLS', '[]', NULL, '2012-02-01', 'Manouba', 6, NULL, NULL, NULL, '2017-10-16 19:25:15', NULL),
(52, 'Latifa', 'Ben Cherif', 'Femme', NULL, '25 963 147', NULL, 'godparent', NULL, NULL, NULL, '74185296', NULL, 'Latifa', '$2y$13$8.BNEjzK9wIDPAKJXpX2sufR49EjzW7OZ8dUBJb8uaz1noGhn2Ire', '[]', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-10-16 20:18:32', NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `intermediate`
--
ALTER TABLE `intermediate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6CF508D641807E1D` (`teacher_id`),
  ADD KEY `IDX_6CF508D623EDC87` (`subject_id`),
  ADD KEY `IDX_6CF508D6DC99104D` (`stclass_id`);

--
-- Index pour la table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9AEACC13B9D69842` (`schoollevel_id`);

--
-- Index pour la table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6A2CA10C38B217A7` (`publication_id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B6BD307FF624B39D` (`sender_id`),
  ADD KEY `IDX_B6BD307FCD53EDB6` (`receiver_id`);

--
-- Index pour la table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_AF3C677961220EA6` (`creator_id`),
  ADD KEY `IDX_AF3C6779C32A47EE` (`school_id`);

--
-- Index pour la table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `stclass`
--
ALTER TABLE `stclass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FB36B49D5FB14BA7` (`level_id`);

--
-- Index pour la table `student_godparent`
--
ALTER TABLE `student_godparent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_431B224ECB944F1A` (`student_id`),
  ADD KEY `IDX_431B224EA84C5BC2` (`godparent_id`);

--
-- Index pour la table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FBCE3E7A5FB14BA7` (`level_id`);

--
-- Index pour la table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_527EDB2523EDC87` (`subject_id`),
  ADD KEY `IDX_527EDB2541807E1D` (`teacher_id`),
  ADD KEY `IDX_527EDB25EA000B10` (`class_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649AA08CB10` (`login`),
  ADD KEY `IDX_8D93D649C32A47EE` (`school_id`),
  ADD KEY `IDX_8D93D649DC99104D` (`stclass_id`),
  ADD KEY `IDX_8D93D649756329DD` (`schoolid_teacher_id`),
  ADD KEY `IDX_8D93D649EF47C51D` (`gdschool_id`),
  ADD KEY `IDX_8D93D649D6C6F3BC` (`stschool_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `intermediate`
--
ALTER TABLE `intermediate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `school`
--
ALTER TABLE `school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `stclass`
--
ALTER TABLE `stclass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `student_godparent`
--
ALTER TABLE `student_godparent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT pour la table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `intermediate`
--
ALTER TABLE `intermediate`
  ADD CONSTRAINT `FK_6CF508D623EDC87` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  ADD CONSTRAINT `FK_6CF508D641807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_6CF508D6DC99104D` FOREIGN KEY (`stclass_id`) REFERENCES `stclass` (`id`);

--
-- Contraintes pour la table `level`
--
ALTER TABLE `level`
  ADD CONSTRAINT `FK_9AEACC13B9D69842` FOREIGN KEY (`schoollevel_id`) REFERENCES `school` (`id`);

--
-- Contraintes pour la table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `FK_6A2CA10C38B217A7` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`id`);

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307FCD53EDB6` FOREIGN KEY (`receiver_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_B6BD307FF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `FK_AF3C677961220EA6` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_AF3C6779C32A47EE` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`);

--
-- Contraintes pour la table `stclass`
--
ALTER TABLE `stclass`
  ADD CONSTRAINT `FK_FB36B49D5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`);

--
-- Contraintes pour la table `student_godparent`
--
ALTER TABLE `student_godparent`
  ADD CONSTRAINT `FK_431B224EA84C5BC2` FOREIGN KEY (`godparent_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_431B224ECB944F1A` FOREIGN KEY (`student_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `subject`
--
ALTER TABLE `subject`
  ADD CONSTRAINT `FK_FBCE3E7A5FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`);

--
-- Contraintes pour la table `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `FK_527EDB2523EDC87` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  ADD CONSTRAINT `FK_527EDB2541807E1D` FOREIGN KEY (`teacher_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_527EDB25EA000B10` FOREIGN KEY (`class_id`) REFERENCES `stclass` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649756329DD` FOREIGN KEY (`schoolid_teacher_id`) REFERENCES `school` (`id`),
  ADD CONSTRAINT `FK_8D93D649C32A47EE` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  ADD CONSTRAINT `FK_8D93D649D6C6F3BC` FOREIGN KEY (`stschool_id`) REFERENCES `school` (`id`),
  ADD CONSTRAINT `FK_8D93D649DC99104D` FOREIGN KEY (`stclass_id`) REFERENCES `stclass` (`id`),
  ADD CONSTRAINT `FK_8D93D649EF47C51D` FOREIGN KEY (`gdschool_id`) REFERENCES `school` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
